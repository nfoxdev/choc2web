package com.nainfox.persistence;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

import lombok.extern.log4j.Log4j;

@Log4j
public class JDBCTests {

	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(Exception e) {
			log.error(e);
		}
		
	}
	
	@Test
	public void testConnection() {
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/securitytest?serverTimezone=Asia/Seoul", "nainfox", "1111");
			log.info(conn);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
}
