package com.nainfox.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nainfox.dao.MemberDao;
import com.nainfox.vo.MemberVo;

import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/config/root-context.xml")
@Log4j
public class MemberServiceTest {

	/*@Autowired
	MemberDao memberDao;
	@Autowired
	JavaMailSender javaMailSender;
	
	@Test
	@Transactional
	public void testAcceptCompanyRegister() {
		
		MemberVo mvo = new MemberVo();
		mvo.setUserid("comtest");
		mvo.setPassword("123456");
		mvo.setEmail1("jjs41580");
		mvo.setEmail2("gmail.com");
		mvo.setName("진수");
		mvo.setCompanyname("나인폭스");
		mvo.setPhone1("010");
		mvo.setPhone2("1234");
		mvo.setPhone3("5678");
		
		log.info("최초 mvo" + mvo.getEmail());
		if(!checkCompanyDual(mvo.getCompanyname())) {
			memberDao.addCompany(mvo);
			int companyid = memberDao.getCompanyidx(mvo.getCompanyname());
			mvo.setCompanyid(companyid);
			memberDao.acceptCompanyRegister(mvo);
			acceptEmailSend(mvo);
			memberDao.deleteWaitRegister(mvo.getUserid());
			//createCompanyAdminTable(mvo);
		}else{
			int companyid = memberDao.getCompanyid(mvo.getCompanyname());
			mvo.setCompanyid(companyid);
			memberDao.acceptCompanyRegister(mvo);
			acceptEmailSend(mvo);
			memberDao.deleteWaitRegister(mvo.getUserid());
		}
		
	}
	
	private boolean checkCompanyDual(String companyName) {
		int result = memberDao.checkCompanyDual(companyName);
		log.info(result);
		if(result == 0) {
			return false;
		}else {
			return true;
		}
	}

	
	public void denyCompanyRegister(MemberVo mvo) {
		// 거절 메일 발송
		// 가입 대기목록에서 삭제
		denyEmailSend(mvo);
		memberDao.deleteWaitRegister(mvo.getUserid());
	}
	
	private void denyEmailSend(MemberVo mvo) {
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(msg, true, "UTF-8");
			helper.setFrom("register@ninefox.com");
			helper.setTo(mvo.getEmail());
			helper.setSubject("CHOC2 통합 관리 서비스 가입이 거절되었습니다.");
			helper.setText("가입이 거절되었습니다. 자세한 사유는 당사에 문의해주시기 바랍니다. 이 주소는 회신이 불가능한 발신전용 메일입니다.");
			javaMailSender.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	@Transactional(propagation=Propagation.MANDATORY)
	private void acceptEmailSend(MemberVo mvo) {
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(msg, true, "UTF-8");
			helper.setFrom("register@ninefox.com");
			helper.setTo(mvo.getEmail());
			helper.setSubject("CHOC2 통합 관리 서비스 가입이 승인되었습니다.");
			helper.setText("가입이 승인되었습니다. 아래 버튼을 통해 로그인해주세요.<br /><button onclick=\"location.href='http://www.choc2.com'\">choc2 로그인하기</button><br />\t이 주소는 회신이 불가능한 발신전용 메일입니다.", true);
			javaMailSender.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	*/
	
}
