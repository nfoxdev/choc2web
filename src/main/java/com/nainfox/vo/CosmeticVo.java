package com.nainfox.vo;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CosmeticVo {
	
	private String id, link, brand_id;
	private boolean functional, func_white, func_wrinkle, func_uv;
	private BrandVo brand;
	private int cosmeticCount;
	
	@Size(min=3, max=50, message="3자이상 50자 이내로 적어주세요.")
	private String kor_name;
	@Size(min=3, max=100, message="3자이상 100자 이내로 적어주세요.")
	private String eng_name;
	@Size(min=5, max=200, message="5자이상 200자 이내로 적어주세요.")
	private String img_path;
	@Pattern(regexp="[0-9]{3,11}", message="숫자만 입력하세요.")
	private String price;
}
