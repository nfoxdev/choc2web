package com.nainfox.vo;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class MemberVo {
	// 'userid','varchar(30)','NO','PRI',NULL,''
	// 'name','varchar(40)','NO','NULL''
	// 'password','varchar(64)','NO','',NULL,''
	// 'phone','varchar(20)','NO','',NULL,''
	// 'email','varchar(50)','NO','UNI',NULL,''
	// 'companyid','int','YES','MUL','1',''
	// companyName varchar(100) not null

	@Pattern(regexp = "[a-zA-Z][a-zA-Z0-9]{3,11}", message = "아이디는 영대소문자로 시작해야하며 영대소문자 혹은 숫자의 혼합으로 3~11글자로 입력하세요")
	private String userid;
	@Size(min = 4, max = 12, message = "패스워드는 4글자 이상 12글자 이하로 입력하세요")
	private String password;
	@Pattern(regexp = "[A-Za-z가-힣]{2,10}", message = "이름은 2글자~10글자로 입력하세요.")
	private String username;
	private String phone;
	private String email;
	private int company_id;
	@Size(min = 1, max = 25, message = "25글자까지 허용됩니다")
	private String companyname;
	//temp_company 테이블에서 조회하기 위한 임시 변수
	@Size(min = 1, max = 25, message = "25글자까지 허용됩니다")
	private String raw_name;
	@Pattern(regexp="[0-9a-zA-Z]([-_.]?[0-9a-zA-Z]){3,15}", message="3~15자의 올바른 이메일 아이디를 입력하세요")
	private String email1;
	@Pattern(regexp="[0-9a-zA-Z]([-_.]?[0-9a-zA-Z]){3,20}\\.[a-zA-Z]{2,3}", message="올바른 도메인 주소를 입력하세요")
	private String email2;
	@Pattern(regexp="01[0-9]", message="비정상적 데이터 접근입니다")
	private String phone1;
	@Pattern(regexp="[0-9]{3,4}", message="휴대폰 번호를 입력하세요")
	private String phone2;
	@Pattern(regexp="[0-9]{4}", message="휴대폰 번호를 입력하세요")
	private String phone3;
	private String regdate;
	private AuthVo auth;
	private MemberDetailVo detail;
	private int code;
	
	public String getRaw_name() {
		return raw_name;
	}

	public void setRaw_name(String raw_name) {
		this.raw_name = raw_name;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getRegdate() {
		return regdate;
	}

	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}

	public AuthVo getAuth() {
		return auth;
	}

	public void setAuth(AuthVo auth) {
		this.auth = auth;
	}

	public MemberDetailVo getDetail() {
		return detail;
	}

	public void setDetail(MemberDetailVo detail) {
		this.detail = detail;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		if(phone1 != null && phone2 != null && phone3 != null) {
			this.phone = phone1 + "-" + phone2 + "-" + phone3;
			return this.phone;
		}else {
			return this.phone;
		}
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		if(email1 != null && email2 != null) {
			this.email = email1 + "@" + email2;
			return this.email;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

}
