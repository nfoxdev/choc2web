package com.nainfox.vo;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class BoardVo {

/*	idx int primary key auto_increment,
    title varchar(200) not null,
    userid varchar(30),
    content longblob,
    hit int,
    ip varchar(30) not null,
    regdate datetime not null,
    companyidx int*/
	
	private int idx;
	@NotEmpty(message="제목을 입력하세요.")
	@Size(max=50, message="제목은 50글자까지 허용됩니다.")
	private String title;
	private String userid;
	@Size(max=1000, message="내용은 1000자까지 허용됩니다.")
	private String content;
	private int hit;
	private String ip;
	private String regdate;
	private int company_id;
	private int notice;
	
	public int getNotice() {
		return notice;
	}
	public void setNotice(int notice) {
		this.notice = notice;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getHit() {
		return hit;
	}
	public void setHit(int hit) {
		this.hit = hit;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	
	
	
	
}
