package com.nainfox.vo;

import java.util.List;

import lombok.Data;

@Data
public class BrandVo {

	private String id, kor_name, eng_name, img_path, link, company_id;
	private List<CosmeticVo> cosmeticList;
}
