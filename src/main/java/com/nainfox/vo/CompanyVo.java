package com.nainfox.vo;

import lombok.Data;

@Data
public class CompanyVo {
	
	/*
	'id','int(11)','NO','PRI',NULL,''
	'country','varchar(5)','YES','',NULL,''
	'type','varchar(10)','YES','',NULL,''
	'classify','varchar(20)','YES','',NULL,''
	'kor_name','varchar(60)','YES','',NULL,''
	'eng_name','varchar(30)','YES','',NULL,''
	'license_num','varchar(20)','YES','',NULL,''
	'link','varchar(40)','YES','',NULL,''
	'raw_class','varchar(40)','YES','',NULL,''
	'raw_name','varchar(60)','YES','',NULL,''
	'owner','varchar(30)','YES','',NULL,''
	'permit_date','date','YES','',NULL,''
	'address','varchar(150)','YES','',NULL,''*/

	
	private int id;
	private String country, type, classify, kor_name, eng_name, license_num, link, raw_class, raw_name, owner, permit_date, address;
	
	
	
}