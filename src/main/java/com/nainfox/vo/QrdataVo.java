package com.nainfox.vo;

public class QrdataVo {

	private int id;
	private String valkey;
	private String val;
	private int quantity;
	private int status;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValkey() {
		return valkey;
	}
	public void setValkey(String valkey) {
		this.valkey = valkey;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
}
