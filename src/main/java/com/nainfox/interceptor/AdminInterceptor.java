package com.nainfox.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nainfox.vo.MemberVo;

public class AdminInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		MemberVo loginMember = (MemberVo)request.getSession().getAttribute("loginMember");
		if(loginMember == null || !loginMember.getAuth().getRole().equals("admin")) {
			request.setAttribute("msg", "잘못된 접근입니다.");
			request.setAttribute("url", "/");
			request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request, response);
			return false;
		}
		return true;
	}

	
	
}
