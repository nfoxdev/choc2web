package com.nainfox.interceptor;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nainfox.service.BrandService;
import com.nainfox.service.CosmeticService;
import com.nainfox.vo.BrandVo;
import com.nainfox.vo.MemberVo;

import lombok.Setter;

public class MyBrandCheckInterceptor extends HandlerInterceptorAdapter{
	
	@Setter
	private BrandService brandService;
	@Setter
	private CosmeticService cosmeticService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		MemberVo member = (MemberVo)request.getSession().getAttribute("loginMember");
		if(member.getAuth().getRole().equals("admin")) {
			return true;
		}
		Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		String paramCompany_id = pathVariables.get("company_id").toString();
		String brand_id =  pathVariables.get("bid").toString();
		
		
		boolean isMyBrand = false;
		//당사의 브랜드가 아닌 파라미터 조작으로 타브랜드의 페이지에 못들어가도록 검증
		List<BrandVo> myBrandList = brandService.getMyBrandList(Integer.parseInt(paramCompany_id));
		for(BrandVo bvo : myBrandList) {
			if(brand_id.equals(bvo.getId())) {
				isMyBrand = true;
				break;
			}
		}
		//회사 소속의 브랜드임이 검증됨
		if(isMyBrand) {
			//화장품 수정/삭제시 파라미터 조작으로 타사의 제품이 변경되는 것을 방지
			if(pathVariables.get("cosmetic_id") != null) {
				String cosmetic_id =  pathVariables.get("cosmetic_id").toString();
				String bidFromCosmetic = cosmeticService.getBrandId(cosmetic_id);
				System.out.println("화장품의 brandid : " + bidFromCosmetic);
				System.out.println("파라미터 brand_id : " + brand_id);
				if(!bidFromCosmetic.equals(brand_id)) {
					request.setAttribute("msg", "올바른 요청이 아닙니다.");
					request.setAttribute("url", "/");
					request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request, response);
					return false;
				}else {
					return true;
				}
			}
			return true;
		}else {
			request.setAttribute("msg", "올바른 요청이 아닙니다.");
			request.setAttribute("url", "/");
			request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request, response);
			return false;
		}
	}

	
}
