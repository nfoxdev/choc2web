package com.nainfox.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nainfox.vo.MemberVo;

public class CompanyPageInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String reqURI = request.getRequestURI().toString();
		MemberVo loginMember = (MemberVo)request.getSession().getAttribute("loginMember");
		if(loginMember == null) {
			request.setAttribute("msg", "로그인 후 이용하세요.");
			request.setAttribute("url", "/login");
			request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request, response);
			return false;
		}
		if(loginMember.getAuth().getRole().equals("admin")) {
			return true;
		}else if(loginMember.getAuth().getRole().equals("company")) {
			Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
			//공지 ajax 검색 페이징 경로는 /company/noticePaging이므로 
			if(reqURI.equals("/company/noticePaging")){
				return true;
			}
			String paramCompany_id = pathVariables.get("company_id").toString();
			
			Integer memberIdx = loginMember.getCompany_id();
			
			boolean isEqualCid = paramCompany_id.equals(memberIdx.toString());
			
			if(!isEqualCid) {
				request.setAttribute("msg", "올바른 요청이 아닙니다.");
				request.setAttribute("url", "/");
				request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request, response);
				return false;
			}else {
				return true;
			}
		}else {
			request.setAttribute("msg", "기업 회원만 이용할 수 있습니다.");
			request.setAttribute("url", "/");
			request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request, response);
			return false;
		}
		
	}
	
}
