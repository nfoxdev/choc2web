package com.nainfox.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class ZxingHelper {

	public static byte[] getQRCodeImage(String text, int width, int height) {
		
		try {
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
			return byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public static void createQRCode(String valkey, int width) throws WriterException, IOException {
		QRCodeWriter q = new QRCodeWriter();
		File file = new File("/Users/nfoxdev04/Documents/qrcodes/"+valkey+".png");
		if(!file.exists()) {
			file.mkdirs();
		}
		try {
			String text = valkey;
			BitMatrix bitMatrix = q.encode(text, BarcodeFormat.QR_CODE, 200, 200);
			MatrixToImageWriter.writeToPath(bitMatrix, "png", file.toPath());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
