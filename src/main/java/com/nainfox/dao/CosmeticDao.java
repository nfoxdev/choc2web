package com.nainfox.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nainfox.vo.CosmeticVo;

@Repository
public class CosmeticDao {

	@Autowired
	private SqlSession session;

	public List<CosmeticVo> getMainitemList() {
		return session.selectList("cosmetics.getMainitemList");
	}

	public int getTotalCountFromBrand(Map<String, Object> searchMap) {
		return session.selectOne("cosmetics.getTotalCountFromBrand", searchMap);
	}

	public List<CosmeticVo> cosmeticListFromBrand(Map<String, Object> searchMap) {
		return session.selectList("cosmetics.getListFromBrand", searchMap);
	}

	public CosmeticVo getCosmetic(String id) {
		return session.selectOne("cosmetics.getCosmetic", id);
	}

	public void addCosmetic(CosmeticVo cvo) {
		session.insert("cosmetics.addCosmetic", cvo);
	}

	public void modifyCosmetic(CosmeticVo cvo) {
		session.update("cosmetics.modifyCosmetic", cvo);
	}

	public String getBrandId(String cosmetic_id) {
		return session.selectOne("cosmetics.getBrandId", cosmetic_id);
	}

	public void deleteCosmetic(String cosmetic_id) {
		session.delete("cosmetics.deleteCosmetic", cosmetic_id);
	}
	
	
	
}
