package com.nainfox.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nainfox.vo.BrandVo;

@Repository
public class BrandDao {

	@Autowired
	private SqlSession session;

	public List<BrandVo> brandSearchPage(Map<String, Object> searchMap) {
		return session.selectList("brands.brandPageList", searchMap);
	}

	public int totalCount(Map<String, Object> searchMap) {
		return session.selectOne("brands.getTotalCount", searchMap);
	}

	public List<BrandVo> getMyBrandList(int company_id) {
		return session.selectList("brands.getMyBrandList", company_id);
	}
	
	public BrandVo getOne(String brand_id) {
		return session.selectOne("brands.selectOne", brand_id);
	}
	
	
	
	
	
}
