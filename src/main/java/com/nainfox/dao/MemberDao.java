package com.nainfox.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nainfox.vo.CompanyVo;
import com.nainfox.vo.MemberVo;

@Repository
public class MemberDao {

	@Autowired
	private SqlSession session;
	
	public int idCheck(String userid) {
		return session.selectOne("member.idCheck", userid);
	}

	public int emailCheck(String email) {
		return session.selectOne("member.emailCheck", email);
	}

	public void memberSignup(MemberVo memberVo) {
		session.insert("member.signup", memberVo);
	}

	public void companyWaitAdd(MemberVo mvo) {
		session.insert("waitRegister.add", mvo);
	}

	public int waitIdCheck(String userid) {
		return session.selectOne("waitRegister.idCheck", userid);
	}

	public int waitEmailCheck(String email) {
		return session.selectOne("waitRegister.emailCheck", email);
	}

	public int checkCompanyDual(String companyName) {
		return session.selectOne("company.dualCheck", companyName);
	}

	public void addCompany(MemberVo mvo) {
		session.insert("company.add", mvo);
	}

	public int getCompanyidx(String companyName) {
		return session.selectOne("company.getIdx", companyName);
	}

	public void deleteWaitRegister(String userid) {
		session.delete("waitRegister.delete", userid);
	}

	public MemberVo findMember(Map<String, String> loginData) {
		return session.selectOne("member.findMember", loginData);
	}
	
	public void addDetail(MemberVo memberVo) {
		session.insert("member.addDetail", memberVo);
	}

	public void addAuth(MemberVo memberVo) {
		session.insert("auth.add", memberVo);
	}

	public List<MemberVo> getWaitRegisterList() {
		return session.selectList("waitRegister.getList");
	}

	public MemberVo getWaitMember(String id) {
		return session.selectOne("waitRegister.findOne", id);
	}

	public String getCompanyname(int companyidx) {
		return session.selectOne("company.getName", companyidx);
	}

	public MemberVo getMemberFromId(String userid) {
		return session.selectOne("member.getMemberFromId", userid);
	}

	public int companyTotalCount() {
		return session.selectOne("company.getTotalCount");
	}

	public List<CompanyVo> companySearchPage(Map<String, Object> searchMap) {
		return session.selectList("company.getPagingList", searchMap);
	}
	
	

	
}
