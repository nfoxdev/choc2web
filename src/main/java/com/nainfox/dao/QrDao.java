package com.nainfox.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nainfox.vo.QrdataVo;

@Repository
public class QrDao {

	@Autowired
	private SqlSession session;
	
	public List<QrdataVo> findAll() {
		return session.selectList("qrdata.AllList");
	}

	public QrdataVo findOne(int id) {
		return session.selectOne("qrdata.findOne", id);
	}

	public void addQRCode(Map<String, String> codeMap) {
		session.insert("qrdata.addQRCode", codeMap);
	}

	public String getValue(String valkey) {
		return session.selectOne("qrdata.getVal", valkey);
	}

	
	
}
