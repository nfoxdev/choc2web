package com.nainfox.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nainfox.vo.BoardVo;

@Repository
public class BoardDao {
	
	@Autowired
	private SqlSession session;
	
	public int totalCount(Map<String, Object> searchMap) {
		return session.selectOne("board.totalCount", searchMap);
	}

	public List<BoardVo> boardSearchPage(Map<String, Object> searchMap) {
		return session.selectList("board.searchPage", searchMap);
	}

	public BoardVo getView(int boardIdx) {
		return session.selectOne("board.getView", boardIdx);
	}

	public void updatePost(BoardVo bvo) {
		session.update("board.updatePost", bvo);
	}

	public void deletePost(int idx) {
		session.delete("board.deletePost", idx);
	}

	public void addPost(BoardVo bvo) {
		session.insert("board.addPost", bvo);
	}

	public List<BoardVo> noticeSearchPage(Map<String, Object> searchMap) {
		return session.selectList("board.noticePageList", searchMap);
	};

}
