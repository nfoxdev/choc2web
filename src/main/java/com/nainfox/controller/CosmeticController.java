package com.nainfox.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.nainfox.service.BrandService;
import com.nainfox.service.CosmeticService;
import com.nainfox.vo.CosmeticVo;

import lombok.Setter;

@Controller
public class CosmeticController {

	@Setter
	private BrandService brandService;
	@Setter
	private CosmeticService cosmeticService;
	
	
	@GetMapping("/cosmetic/{id}")
	public String viewCosmetic(@PathVariable String id, Model model) {
		CosmeticVo cvo = cosmeticService.getCosmetic(id);
		model.addAttribute("cosmeticVo", cvo);
		System.out.println(cvo.getPrice());
		return "/cosmetics/view.jsp";
	}
	
	
}
