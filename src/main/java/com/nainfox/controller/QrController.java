package com.nainfox.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.zxing.WriterException;
import com.nainfox.service.QrService;
import com.nainfox.util.ZxingHelper;
import com.nainfox.vo.QrdataVo;

import lombok.Setter;

@Controller
public class QrController {

	@Setter
	private QrService qrService;

	@GetMapping("/qr/list")
	public String viewList(Model model) {
		model.addAttribute("qrList", qrService.findAll());
		return "/qr/list.jsp";
	}

	//스트림으로 생성 후 출력 (파일 불러오는 로직 아님)
	@GetMapping("/qr/{id}")
	public void qrImgView(@PathVariable int id, HttpServletResponse response) throws Exception {
		QrdataVo qvo = qrService.findOne(id);
		response.setContentType("image/png");
		OutputStream outputStream = response.getOutputStream();
		outputStream.write(ZxingHelper.getQRCodeImage(qvo.getValkey(), 200, 200));
		outputStream.flush();
		outputStream.close();
	}
	
	//Ajax로 QR코드 임의생성시 컨트롤러
	@PostMapping("/qr/create")
	@ResponseBody
	public String qrCodeCreate(@RequestParam String vkey, @RequestParam String val, 
			@RequestParam(required=false, defaultValue="1") String quantity) throws WriterException, IOException {
		qrService.addQRCode(vkey, val, quantity);
		return "success";
	}
	
	//QR코드 스캔후 post요청시 (api)
	@PostMapping("/qr/scan")
	public String qrScanReturn(@RequestParam String valkey) {
		String url = qrService.getValue(valkey);
		return "redirect:"+url;
	}
	
	
}
