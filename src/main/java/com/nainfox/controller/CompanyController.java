package com.nainfox.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.zxing.WriterException;
import com.nainfox.service.BoardService;
import com.nainfox.service.BrandService;
import com.nainfox.service.CosmeticService;
import com.nainfox.service.MemberService;
import com.nainfox.vo.BoardVo;
import com.nainfox.vo.BrandVo;
import com.nainfox.vo.CosmeticVo;
import com.nainfox.vo.MemberVo;

import lombok.Setter;

@Controller
public class CompanyController {
	
	@Setter
	private MemberService memberService;
	@Setter
	private BoardService boardService;
	@Setter
	private BrandService brandService;
	@Setter
	private CosmeticService cosmeticService;

	//기업 게시물 리스트출력
	@GetMapping(value="/company/{company_id}")
	public String companyBoardPostList(Model model, @PathVariable int company_id) {
		String companyname = memberService.getCompanyname(company_id);
		String viewURI = "/company/"+company_id+"/board/view";
		//해당 기업의 브랜드리스트 가져와야함
		List<BrandVo> myBrandList = brandService.getMyBrandList(company_id);
		//공지 게시판 검색 및 페이징용 맵
		Map<String, Object> boardMap = new HashMap<>();
		boardMap.put("keyword", "");
		boardMap.put("page", 1);
		boardMap.put("option", "all");
		boardMap.put("company_id", company_id);
		
		///보낼 데이터들
		Map<String, Object> resultMap = boardService.noticeSearchPageList(boardMap);
		resultMap.put("companyname", companyname);
		resultMap.put("brandList", myBrandList);
		
		model.addAttribute("companyMap", resultMap);
		model.addAttribute("viewURI", viewURI);
		return "/company/main.jsp";
	}
	
	//ajax 페이징
	@PostMapping("/company/noticePaging")
	@ResponseBody
	public Map<String, Object> boardPaging(@RequestParam String keyword, @RequestParam int page,
			@RequestParam String option, @RequestParam(required=false) Integer company_id){
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", keyword);
		searchMap.put("page", page);
		searchMap.put("option", option);
		if(company_id != null) {
			searchMap.put("company_id", company_id);
		}
		Map<String, Object> resultMap = boardService.boardSearchPageList(searchMap);
		return resultMap;
	}
	
	//기업 게시물 조회
	@GetMapping("/company/{company_id}/board/view")
	public String companyPostView(Model model, @PathVariable int company_id, @RequestParam int id, HttpSession session) {
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		BoardVo bvo = boardService.getView(id);
		String listURI = "/company/"+company_id;
		String modURI = "/company/"+company_id+"/board/mod?id="+id;
		String deleteURI = "/company/"+company_id+"/board/del";
		System.out.println("공지여부 : "+bvo.getNotice());
		//게시물의 company_id와 요청자의 company_id가 일치하지 않을 경우 막아야함
		if(bvo.getCompany_id() != requestMember.getCompany_id() && !requestMember.getAuth().getRole().contains("admin") 
				&& bvo.getNotice() != 1) {
			model.addAttribute("msg", "올바른 요청이 아닙니다.");
			model.addAttribute("url", listURI);
			return "/error.jsp";
		}
		model.addAttribute("board", bvo);
		model.addAttribute("listURI", listURI);
		model.addAttribute("modURI", modURI);
		model.addAttribute("deleteURI", deleteURI);
		return "/company/view.jsp";
	}
	
	//기업 게시물 수정
	@GetMapping("/company/{company_id}/board/mod")
	public String companyPostModifyForm(Model model, @PathVariable int company_id, @RequestParam int id, HttpSession session) {
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		BoardVo bvo = boardService.getView(id);
		String cancelURI = "/company/"+company_id+"/board/view?id="+id;
		String actionURI = "/company/"+company_id+"/board/mod";
		
		if(!requestMember.getUserid().equals(bvo.getUserid())) {
			model.addAttribute("msg", "타인의 게시글은 수정 불가능합니다.");
			model.addAttribute("url", cancelURI);
			return "/error.jsp";
		}
		model.addAttribute("boardVo", bvo);
		model.addAttribute("cancelURI", cancelURI);
		model.addAttribute("actionURI", actionURI);
		return "/company/modify.jsp";
	}
	
	@PostMapping("/company/{company_id}/board/mod")
	public String companyPostModify(@ModelAttribute @Valid BoardVo bvo, BindingResult result, Model model, 
			@PathVariable int company_id, HttpSession session) {
		//히든태그 idx value 변조시 대응
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		//수정요청한 원본글 userid를 DB에서 참조 (userid 변조까지 고려)
		String originPostUserid = boardService.getView(bvo.getIdx()).getUserid();
		if(!requestMember.getUserid().equals(originPostUserid)) {
			model.addAttribute("msg", "올바른 요청이 아닙니다.");
			model.addAttribute("url", "/");
			return "/error.jsp";
		}
		//정규표현식 적용
		if(result.hasErrors()) {
			String cancelURI = "/company/"+company_id+"/board/view?id="+bvo.getIdx();
			String actionURI = "/company/"+company_id+"/board/mod";
			model.addAttribute("cancelURI", cancelURI);
			model.addAttribute("actionURI", actionURI);
			model.addAttribute("boardVo", bvo);
			return "/board/modify.jsp";
		}
		boardService.modify(bvo);
		return "redirect:/company/"+company_id+"/board/view?id="+bvo.getIdx();
	}
	
	@PostMapping("/company/{company_id}/board/del")
	@ResponseBody
	public String companyPostDelete(@RequestParam int idx, HttpSession session) {
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		BoardVo originPost = boardService.getView(idx);
		if(!requestMember.getAuth().getRole().equals("admin") && !requestMember.getUserid().equals(originPost.getUserid())) {
			return "denied";
		}
		boardService.deleteView(idx);
		return "done";
	}
	
	@GetMapping("/company/{company_id}/board/add")
	public String companyPostAddForm(Model model, @PathVariable int company_id, HttpSession session) {
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		BoardVo bvo = new BoardVo();
		String cancelURI = "/company/"+company_id;
		String actionURI = "/company/"+company_id+"/board/add";
		bvo.setUserid(requestMember.getUserid());
		model.addAttribute("boardVo", bvo);
		model.addAttribute("cancelURI", cancelURI);
		model.addAttribute("actionURI", actionURI);
		return "/company/add.jsp";
	}
	
	@PostMapping("/company/{company_id}/board/add")
	public String companyPostAdd(@ModelAttribute @Valid BoardVo bvo, BindingResult result, Model model, 
			@PathVariable int company_id, HttpSession session, HttpServletRequest request) throws WriterException, IOException {
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		if(result.hasErrors()) {
			String cancelURI = "/company/"+company_id;
			String actionURI = "/company/"+company_id+"/board/add";
			model.addAttribute("cancelURI", cancelURI);
			model.addAttribute("actionURI", actionURI);
			model.addAttribute("boardVo", bvo);
			return "/company/add.jsp";
		}
		bvo.setUserid(requestMember.getUserid());
		//관리자만 타기업 게시판에 글을 남길 수 있도록 조치
		if(requestMember.getAuth().getRole().equals("admin")) {
			bvo.setCompany_id(company_id);
		}else {
			bvo.setCompany_id(requestMember.getCompany_id());
		}
		bvo.setIp(request.getRemoteAddr());
		
		boardService.add(bvo);
		
		return "redirect:/company/"+company_id;
	}
	
	//제품 관리 페이지 당사브랜드검증은 인터셉터 처리
	@GetMapping("/company/{company_id}/brand/{bid}")
	public String viewMyCosmeticList(@PathVariable int company_id, @PathVariable String bid, Model model, HttpSession session) {
		session.setAttribute("brand", (BrandVo)brandService.getOneBrand(bid));
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", "");
		searchMap.put("page", 1);
		searchMap.put("brand_id", bid);
		Map<String, Object> resultMap = cosmeticService.getCosmeticListFromBrand(searchMap);
		model.addAttribute("resultMap", resultMap);
		return "/company/cosManage.jsp";
	}
	
	@GetMapping("/company/{company_id}/brand/{bid}/cosmetic/add")
	public String addCosmeticForm(@PathVariable int company_id, @PathVariable String bid, Model model, HttpSession session) {
		CosmeticVo cvo = new CosmeticVo();
		String actionURI = "/company/"+company_id+"/brand/"+bid+"/cosmetic/add";
		String cancelURI = "/company/"+company_id+"/brand/"+bid;
		session.setAttribute("brand", (BrandVo)brandService.getOneBrand(bid));
		model.addAttribute("actionURI", actionURI);
		model.addAttribute("cancelURI", cancelURI);
		model.addAttribute("cosmeticVo", cvo);
		return "/company/addCosmetic.jsp";
	}
	
	@PostMapping("/company/{company_id}/brand/{bid}/cosmetic/add")
	public String addCosmetic(@PathVariable int company_id, @PathVariable String bid, Model model,
			@ModelAttribute @Valid CosmeticVo cvo, BindingResult result, HttpSession session) {
		if(result.hasErrors()) {
			String actionURI = "/company/"+company_id+"/brand/"+bid+"/cosmetic/add";
			String cancelURI = "/company/"+company_id+"/brand/"+bid;
			model.addAttribute("actionURI", actionURI);
			model.addAttribute("cancelURI", cancelURI);
			return "/company/addCosmetic.jsp";
		}
		BrandVo bvo = (BrandVo)session.getAttribute("brand");
		cvo.setBrand_id(bvo.getId());
		cosmeticService.addCosmetic(cvo);
		return "redirect:/company/"+company_id+"/brand/"+bid;
	}
	
	@GetMapping("/company/{company_id}/brand/{bid}/cosmetic/mod/{cosmetic_id}")
	public String modifyCosmeticForm(@PathVariable int company_id, @PathVariable String bid, @PathVariable String cosmetic_id, 
			Model model, HttpSession session) {
		CosmeticVo cosmeticVo = cosmeticService.getCosmetic(cosmetic_id);
		String actionURI = "/company/"+company_id+"/brand/"+bid+"/cosmetic/mod/"+cosmetic_id;
		String cancelURI = "/company/"+company_id+"/brand/"+bid;
		session.setAttribute("brand", (BrandVo)brandService.getOneBrand(bid));
		model.addAttribute("cosmeticVo", cosmeticVo);
		model.addAttribute("actionURI", actionURI);
		model.addAttribute("cancelURI", cancelURI);
		return "/company/addCosmetic.jsp";
	}
	
	@PostMapping("/company/{company_id}/brand/{bid}/cosmetic/mod/{cosmetic_id}")
	public String modifyCosmetic(@PathVariable int company_id, @PathVariable String bid, @PathVariable String cosmetic_id, 
			Model model, @ModelAttribute @Valid CosmeticVo cvo, BindingResult result, HttpSession session) {
		if(result.hasErrors()) {
			String actionURI = "/company/"+company_id+"/brand/"+bid+"/cosmetic/mod/"+cosmetic_id;
			String cancelURI = "/company/"+company_id+"/brand/"+bid;
			model.addAttribute("actionURI", actionURI);
			model.addAttribute("cancelURI", cancelURI);
			return "/company/addCosmetic.jsp";
		}
		BrandVo bvo = (BrandVo)session.getAttribute("brand");
		cvo.setBrand_id(bvo.getId());
		cvo.setId(cosmetic_id);
		cosmeticService.modifyCosmetic(cvo);
		return "redirect:/company/"+company_id+"/brand/"+bid;
	}
	
	@PostMapping("/company/{company_id}/brand/{bid}/cosmetic/del/{cosmetic_id}")
	@ResponseBody
	public String deleteCosmetic(@PathVariable int company_id, @PathVariable String bid, @PathVariable String cosmetic_id, 
			 HttpSession session) {
		cosmeticService.deleteCosmetic(cosmetic_id);
		return "success";
	}
	
	
	
	
	
}