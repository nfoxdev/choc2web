package com.nainfox.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nainfox.exception.NoDataFoundException;
import com.nainfox.service.IFormService;
import com.nainfox.service.MemberService;
import com.nainfox.vo.MemberVo;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Controller
@Log4j
public class MemberController {
	
	@Setter
	private MemberService memberService;
	@Setter
	private IFormService iFormService;
	
	@GetMapping("/login")
	public String loginForm() {
		return "/member/login.jsp";
	}
	
	@PostMapping("/login")
	public String login(@RequestParam String userid, @RequestParam String password, HttpServletRequest request, Model model,
			@RequestParam(required=false, defaultValue="/") String prevURL) {
		MemberVo findMember = memberService.findMember(userid, password);
		if(findMember != null) {
			request.getSession().invalidate();
			request.getSession().setAttribute("loginMember", findMember);
			log.warn("회원 로그인\n" + "ID : " + findMember.getUserid() + "\n" + 
			"IP : " + request.getRemoteAddr());
			if(prevURL.contains("/login")){
				return "redirect:/";
			}
			return "redirect:"+prevURL;
		}else{
			model.addAttribute("msg", "아이디 혹은 비밀번호가 틀립니다.");
			model.addAttribute("url", "/login");
			return "/error.jsp";
		}
	}
	
	@GetMapping("/loginjson")
	public String loginJSONForm() {
		return "/member/loginjson.jsp";
	}
	
	@PostMapping("/loginjson")
	@ResponseBody
	public MemberVo loginJSON(@RequestParam String userid, @RequestParam String password, HttpServletRequest request, Model model,
			@RequestParam(required=false, defaultValue="/") String prevURL) throws NoDataFoundException {
		MemberVo findMember = memberService.findMember(userid, password);
		if(findMember != null) {
			findMember.setCode(200);
			return findMember;
		}else {
			throw new NoDataFoundException("데이터를 찾을 수 없습니다.");
		}
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpSession session, Model model) {
		MemberVo mvo = (MemberVo)session.getAttribute("loginMember");
		if(mvo == null) {
			model.addAttribute("msg", "로그인하지 않은 상태입니다.");
			model.addAttribute("url", "/");
			return "/error.jsp";
		}else {
			log.warn("회원 로그아웃\n" + "ID : " + mvo.getUserid() + "\n" + 
					"IP : " + request.getRemoteAddr());
			request.getSession().invalidate();
			return "redirect:/";
		}
	}
	
	@GetMapping("/signup")
	public String signupForm() {
		return "/member/signup/signup.jsp";
	}
	
	@GetMapping("/signup/company")
	public String companyForm(Model model) {
		//spring form 태그 이용하기 위한 객체 전달
		MemberVo mvo = new MemberVo();
		Map<String, String> phoneMap = iFormService.getPhoneMap();
		model.addAttribute("memberVo", mvo);
		model.addAttribute("phoneMap", phoneMap);
		return "/member/signup/company.jsp";
	}
	
	@GetMapping("/signup/member")
	public String memberForm(Model model) {
		MemberVo mvo = new MemberVo();
		Map<String, String> phoneMap = iFormService.getPhoneMap();
		model.addAttribute("memberVo", mvo);
		model.addAttribute("phoneMap", phoneMap);
		return "/member/signup/member.jsp";
	}
	
	@PostMapping("/signup/company")
	public String companySignup(@ModelAttribute @Valid MemberVo memberVo, BindingResult result, Model model) {
		try {
			if(memberService.idCheck(memberVo.getUserid()).equals("dual")) {
				FieldError error = new FieldError("idDual", "userid", "중복된 아이디입니다.");
				result.addError(error);
			}
			if(memberService.emailCheck(memberVo.getEmail()).equals("dual")) {
				FieldError error = new FieldError("emailDual", "email", "중복된 이메일입니다.");
				result.addError(error);
			}
			if(memberVo.getPhone1() == null || memberVo.getPhone2() == null || memberVo.getPhone3() == null) {
				FieldError error = new FieldError("phoneNull", "phone", "휴대폰 번호를 입력하세요");
				result.addError(error);
			}
			if(memberVo.getEmail1() == null || memberVo.getEmail2() == null) {
				FieldError error = new FieldError("emailNull", "email", "이메일을 입력하세요");
				result.addError(error);
			}
			if(result.hasErrors()) {
				model.addAttribute("phoneMap", iFormService.getPhoneMap());
				model.addAttribute("memberVo", memberVo);
				return "/member/signup/company.jsp";
			}else {
				memberService.companyWaitAdd(memberVo);
				model.addAttribute("url", "/");
				model.addAttribute("msg", "회원가입을 요청했습니다. 관리자가 승인하면 회원가입이 완료됩니다. 승인 결과는 메일로 알려드립니다.");
				return "/error.jsp";
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			model.addAttribute("url", "/signup");
			model.addAttribute("msg", "서버 오류입니다. 잠시 후 다시 시도하세요");
			return "/error.jsp";
		}
	}
	
	@PostMapping("/signup/member")
	public String memberSignup(@ModelAttribute @Valid MemberVo memberVo, BindingResult result, Model model) {
		try {
			if(memberService.idCheck(memberVo.getUserid()).equals("dual")) {
				FieldError error = new FieldError("idDual", "userid", "중복된 아이디입니다.");
				result.addError(error);
			}
			if(memberService.emailCheck(memberVo.getEmail()).equals("dual")) {
				FieldError error = new FieldError("emailDual", "email", "중복된 이메일입니다.");
				result.addError(error);
			}
			if(memberVo.getPhone1() == null || memberVo.getPhone2() == null || memberVo.getPhone3() == null) {
				FieldError error = new FieldError("phoneNull", "phone", "휴대폰 번호를 입력하세요");
				result.addError(error);
			}
			if(memberVo.getEmail1() == null || memberVo.getEmail2() == null) {
				FieldError error = new FieldError("emailNull", "email", "이메일을 입력하세요");
				result.addError(error);
			}
			if(result.hasErrors()) {
				model.addAttribute("phoneMap", iFormService.getPhoneMap());
				model.addAttribute("memberVo", memberVo);
				return "/member/signup/member.jsp";
			}else {
				memberService.memberSignup(memberVo);
				model.addAttribute("url", "/");
				model.addAttribute("msg", "회원가입이 완료되었습니다. 메인페이지로 이동합니다.");
				return "/error.jsp";
			}
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("url", "/signup");
			model.addAttribute("msg", "서버 오류입니다. 잠시 후 다시 시도하세요");
			return "/error.jsp";
		}
	}
	
	@PostMapping("/signup/idCheck")
	@ResponseBody
	public String idCheck(@RequestParam String userid) {
		try {
			String result = memberService.idCheck(userid);
			return result;
		}catch (Exception e) {
			log.warn("아이디 중복확인 오류 : " + e.getMessage());
			return "fail";
		}
	}
	
	@PostMapping("/signup/companyIdCheck")
	@ResponseBody
	public String companyIdCheck(@RequestParam String userid) {
		try {
			String result = memberService.idCheck(userid);
			return result;
		}catch (Exception e) {
			log.warn("아이디 중복확인 오류 : " + e.getMessage());
			return "fail";
		}
	}
	
	
	@PostMapping("/signup/accept")
	@ResponseBody
	public boolean acceptCompanyRegister(@RequestBody MemberVo mvo, HttpServletRequest request) {
		try {
			//waitRegister의 기업명의 컬럼명이 companyname, 맵핑 컬럼명 임시 테이블 raw_name인 점
			mvo.setRaw_name(mvo.getCompanyname());
			memberService.acceptCompanyRegister(mvo);
			log.warn("기업 회원 승인\n" + "IP : " + request.getRemoteAddr());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("기업 회원가입 오류 : " + e.getMessage());
			return false;
		}
	}
	
	@PostMapping("/signup/deny")
	@ResponseBody
	public boolean denyCompanyRegister(@RequestBody MemberVo mvo, HttpServletRequest request) {
		try {
			memberService.denyCompanyRegister(mvo);
			log.warn("기업 회원 거절\n" + "IP : " + request.getRemoteAddr());
			return true;
		} catch (RuntimeException e) {
			e.printStackTrace();
			log.error("기업 회원가입 오류 : " + e.getMessage());
			return false;
		}
	}

	
	
	
	
}
