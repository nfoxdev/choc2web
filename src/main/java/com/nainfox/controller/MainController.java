package com.nainfox.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.nainfox.service.BrandService;
import com.nainfox.service.CosmeticService;
import com.nainfox.vo.CosmeticVo;

import lombok.Setter;

@Controller
public class MainController {
	
	@Setter
	private CosmeticService cosmeticService;
	
	@Setter
	private BrandService brandService;
	
	@GetMapping("/")
	public String main(Model model) {
		List<CosmeticVo> cosmeticList = cosmeticService.getMainitemList();
		model.addAttribute("cosmeticList", cosmeticList);
		return "/main.jsp";
	}
	
	
	
	
}
