package com.nainfox.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nainfox.service.BrandService;
import com.nainfox.service.CosmeticService;
import com.nainfox.vo.BrandVo;

import lombok.Setter;

@Controller
public class BrandController {

	@Setter
	private BrandService brandService;
	@Setter
	private CosmeticService cosmeticService;
	
	
	@GetMapping("/brand")
	public String brandSearchList(Model model) {
		String viewURI = "/brand/view";
		Map<String, Object> brandMap = new HashMap<>();
		brandMap.put("keyword", "");
		brandMap.put("page", 1);
		Map<String, Object> resultMap = brandService.brandSearchPageList(brandMap);
		model.addAttribute("brandMap", resultMap);
		model.addAttribute("viewURI", viewURI);
		return "/brands/list.jsp";
	}
	
	@GetMapping("/brand/view")
	public String cosmeticListViewFromBrand(@RequestParam String id, Model model, HttpSession session) {
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", "");
		searchMap.put("page", 1);
		searchMap.put("brand_id", id);
		Map<String, Object> resultMap = cosmeticService.getCosmeticListFromBrand(searchMap);
		session.setAttribute("brand", (BrandVo)brandService.getOneBrand(id));
		model.addAttribute("resultMap", resultMap);
		return "/brands/view.jsp";
	}
	
	@PostMapping("/brand/brandPaging")
	@ResponseBody
	public Map<String, Object> brandPaging(@RequestParam String keyword, @RequestParam int page){
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", keyword);
		searchMap.put("page", page);
		Map<String, Object> resultMap = brandService.brandSearchPageList(searchMap);
		return resultMap;
	}
	
	@PostMapping("/brand/cosmeticPaging")
	@ResponseBody
	public Map<String, Object> cosmeticPaging(@RequestParam String keyword, @RequestParam int page, @RequestParam String brand_id){
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", keyword);
		searchMap.put("page", page);
		searchMap.put("brand_id", brand_id);
		Map<String, Object> resultMap = cosmeticService.getCosmeticListFromBrand(searchMap);
		return resultMap;
	}
	
	
}
