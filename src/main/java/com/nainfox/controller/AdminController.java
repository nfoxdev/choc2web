package com.nainfox.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.zxing.WriterException;
import com.nainfox.service.BoardService;
import com.nainfox.service.MemberService;
import com.nainfox.vo.BoardVo;
import com.nainfox.vo.MemberVo;

import lombok.Setter;

@Controller
public class AdminController {
	
	@Setter
	private MemberService memberService;
	@Setter
	private BoardService boardService;
	
	@GetMapping("/admin")
	public String adminForm() {
		return "/admin/admin.jsp";
	}
	
	@PostMapping("/admin/waitlist")
	@ResponseBody
	public List<MemberVo> getWaitList(){
		List<MemberVo> waitList = memberService.getWaitList();
		return waitList;
	}
	
	@GetMapping("/admin/eval")
	public String evalForm(@RequestParam String id, Model model, HttpSession session) {
		MemberVo evalMember = memberService.getWaitMember(id);
		session.setAttribute("companyUserid", evalMember.getUserid());
		session.setAttribute("companyPass", evalMember.getPassword());
		session.setAttribute("companyUsername", evalMember.getUsername());
		model.addAttribute("memberVo", evalMember);
		return "/admin/eval.jsp?id="+id;
	}
	
	@PostMapping("/admin/eval")
	public String evaluate(@ModelAttribute MemberVo mvo, BindingResult result, Model model, HttpSession session) {
		mvo.setUserid((String)session.getAttribute("companyUserid"));
		mvo.setUsername((String)session.getAttribute("companyUsername"));
		mvo.setPassword((String)session.getAttribute("companyPass"));
		session.removeAttribute("companyUserid");
		session.removeAttribute("companyPass");
		session.removeAttribute("companyUsername");
		try {
			memberService.acceptCompanyRegister(mvo);
			model.addAttribute("msg", mvo.getUserid() + "님의 기업회원 가입을 승인했습니다.");
			model.addAttribute("url", "/admin");
			return "/error.jsp";
		} catch (SQLException e) {
			model.addAttribute("msg", "서버 에러입니다. 다시 시도해주세요.");
			model.addAttribute("url", "/admin");
			return "/error.jsp";
		}
	}
	
	@GetMapping("/admin/announce")
	public String announceForm(Model model) {
		BoardVo boardVo = new BoardVo();
		String cancelURI = "/admin";
		String actionURI = "/admin/announce";
		model.addAttribute("boardVo", boardVo);
		model.addAttribute("cancelURI", cancelURI);
		model.addAttribute("actionURI", actionURI);
		return "/admin/announce.jsp";
	}
	
	@PostMapping("/admin/announce")
	public String addAnnounce(@ModelAttribute @Valid BoardVo boardVo, BindingResult result, Model model,
			HttpSession session, HttpServletRequest request) throws WriterException, IOException {
		MemberVo requestMember = (MemberVo)session.getAttribute("loginMember");
		if(result.hasErrors()) {
			String cancelURI = "/admin";
			String actionURI = "/admin/announce";
			model.addAttribute("cancelURI", cancelURI);
			model.addAttribute("actionURI", actionURI);
			model.addAttribute("boardVo", boardVo);
			return "/admin/announce.jsp";
		}
		boardVo.setNotice(1);
		boardVo.setUserid(requestMember.getUserid());
		boardVo.setIp(request.getRemoteAddr());
		
		boardService.add(boardVo);
		model.addAttribute("url", "/admin");
		model.addAttribute("msg", "전체 공지를 등록하였습니다.");
		return "redirect:/admin";
	}
	
	@GetMapping("/admin/company/list")
	public String companyListView(Model model) {
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", "");
		searchMap.put("page", 1);
		Map<String, Object> resultMap = memberService.getCompanyList(searchMap);
		model.addAttribute("companyMap", resultMap);
		return "/admin/company.jsp";
	}
	
	@PostMapping("/admin/companyPaging")
	@ResponseBody
	public Map<String, Object> adminCompanyPaging(@RequestParam String keyword, @RequestParam int page){
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("keyword", keyword);
		searchMap.put("page", page);
		Map<String, Object> resultMap = memberService.getCompanyList(searchMap);
		return resultMap;
	}
	
}
