package com.nainfox.exception;

@SuppressWarnings("serial")
public class NoDataFoundException extends Exception {

	private final int CODE;// 생성자를 통해 초기화 한다.

	public NoDataFoundException(String msg, int code){ //생성자
		super(msg);
		CODE=code;
	}

	public NoDataFoundException(String msg){// 생성자
		this(msg, 100);// ERR_CODE를 100(기본값)으로 초기화한다.
	}

	public int getCode(){// 에러 코드를 얻을 수 있는 메서드도 추가한다.
		return CODE;// 이 메서드는 주로 getMessage()와 함께 사용될 것이다.
	}
	
	
}
