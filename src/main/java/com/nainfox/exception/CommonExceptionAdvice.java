package com.nainfox.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.log4j.Log4j;

@ControllerAdvice
@Log4j
public class CommonExceptionAdvice {

	@ExceptionHandler(Exception.class)
	public void exceptionProcess(Exception e) {
		e.printStackTrace();
		System.out.println("::::::::::공통예외:::::::::::");
		/*HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session = req.getSession();
		MemberVo member = (MemberVo)(session.getAttribute("loginMember"));
		String memberId = null;
		Date date = new Date();
		if(member != null) {
			memberId = member.getUserid();
		}
		StringBuffer sb = new StringBuffer();
		
		sb.append("\n-------------START stacktrace -------------\n");
		for(StackTraceElement s : e.getStackTrace()) {
			sb.append(s + "\n");
		}
		sb.append("----------------END stacktrace -------------\n");
		sb.append("ID : " + memberId + "\n");
		sb.append("요청 경로 : " + req.getRequestURI() + "\n");
		sb.append("이전 경로 : " + req.getHeader("referer") + "\n");
		sb.append("예외 메세지 : " + e.getMessage() + "\n");
		sb.append("처리 메서드 : " + e.getClass().getSimpleName() + "\n");
		sb.append("IP : " + req.getRemoteAddr() + "\n");
		sb.append("처리 시간 : " + date);
		log.error(sb.toString());*/
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseBody
	public Map<String, Object> apiException(MissingServletRequestParameterException e) {
		Map<String, Object> returnMap = new HashMap<>();
		returnMap.put("msg", "Parameter Value Error");
		returnMap.put("cause", e.getMessage().toString());
		returnMap.put("errcode", 100);
		return returnMap;
	}
	
	@ExceptionHandler(NoDataFoundException.class)
	@ResponseBody
	public Map<String, Object> noDataFoundException(NoDataFoundException e) {
		Map<String, Object> returnMap = new HashMap<>();
		returnMap.put("msg", "NoDataFoundException");
		returnMap.put("cause", e.getMessage().toString());
		returnMap.put("errcode", e.getCode());
		return returnMap;
	}
	
	
	
}
