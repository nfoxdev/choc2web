package com.nainfox.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.zxing.WriterException;
import com.nainfox.dao.QrDao;
import com.nainfox.util.ZxingHelper;
import com.nainfox.vo.QrdataVo;

import lombok.Setter;

@Service
public class QrServiceImpl implements QrService{

	@Setter
	private QrDao qrDao;
	
	public List<QrdataVo> findAll(){
		return qrDao.findAll();
	}

	@Override
	public QrdataVo findOne(int id) {
		return qrDao.findOne(id);
	}

	@Override
	public void addQRCode(String vkey, String val, String quantity) throws WriterException, IOException {
		ZxingHelper.createQRCode(vkey, 200);
		Map<String, String> codeMap = new HashMap<>();
		codeMap.put("valkey", vkey);
		codeMap.put("val", val);
		if(quantity != null) {
			codeMap.put("quantity", quantity);
		}
		qrDao.addQRCode(codeMap);
	}

	@Override
	public String getValue(String valkey) {
		return qrDao.getValue(valkey);
	}
	
	
	
	
	
}
