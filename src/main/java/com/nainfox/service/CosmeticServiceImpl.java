package com.nainfox.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.nainfox.dao.CosmeticDao;
import com.nainfox.vo.CosmeticVo;

import lombok.Setter;

@Service
public class CosmeticServiceImpl implements CosmeticService{

	@Setter
	private CosmeticDao cosmeticDao;
	private static final int maxCountOfOneList = 30;
	
	@Override
	public List<CosmeticVo> getMainitemList() {
		List<CosmeticVo> cosmeticList = cosmeticDao.getMainitemList();
		return cosmeticList;
	}

	@Override
	public Map<String, Object> getCosmeticListFromBrand(Map<String, Object> searchMap) {
		//브랜드 선택 기준 해당 브랜드의 총 제품 개수 파악
		int totalCount = cosmeticDao.getTotalCountFromBrand(searchMap);
		int page = (int)(searchMap.get("page"));
		int startRownum = (page - 1) * maxCountOfOneList + 1;
		int endRownum = startRownum + (maxCountOfOneList - 1);
		
		searchMap.put("total", totalCount);
		searchMap.put("start", startRownum);
		searchMap.put("end", endRownum);
		
		List<CosmeticVo> cosmeticList = cosmeticDao.cosmeticListFromBrand(searchMap);
		searchMap.put("cosmeticList", cosmeticList);
		searchMap.put("totalCount", totalCount);
		int totalPage = 0;
		
		if(totalCount % maxCountOfOneList == 0) {
			totalPage = totalCount / maxCountOfOneList;
		}else {
			totalPage = (totalCount / maxCountOfOneList) + 1;
		}
		
		searchMap.put("totalPage", totalPage);
		return searchMap;
	}

	@Override
	public CosmeticVo getCosmetic(String id) {
		return cosmeticDao.getCosmetic(id);
	}

	@Override
	public void addCosmetic(CosmeticVo cvo) {
		cosmeticDao.addCosmetic(cvo);
	}

	@Override
	public void modifyCosmetic(CosmeticVo cvo) {
		cosmeticDao.modifyCosmetic(cvo);
	}

	@Override
	public String getBrandId(String cosmetic_id) {
		String brand_id = cosmeticDao.getBrandId(cosmetic_id);
		return brand_id;
	}

	@Override
	public void deleteCosmetic(String cosmetic_id) {
		cosmeticDao.deleteCosmetic(cosmetic_id);
	}
	
	
	
}
