package com.nainfox.service;

import java.util.List;
import java.util.Map;

import com.nainfox.vo.CosmeticVo;

public interface CosmeticService {

	List<CosmeticVo> getMainitemList();

	Map<String, Object> getCosmeticListFromBrand(Map<String, Object> searchMap);

	CosmeticVo getCosmetic(String id);

	void addCosmetic(CosmeticVo cvo);

	void modifyCosmetic(CosmeticVo cvo);

	String getBrandId(String cosmetic_id);

	void deleteCosmetic(String cosmetic_id);
	
	
}
