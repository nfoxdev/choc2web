package com.nainfox.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.nainfox.vo.MemberVo;

public interface MemberService {

	String idCheck(String userid) throws SQLException;

	String emailCheck(String email) throws SQLException;

	void memberSignup(MemberVo memberVo) throws SQLException;

	void companyWaitAdd(MemberVo mvo);

	void acceptCompanyRegister(MemberVo mvo) throws SQLException;

	void denyCompanyRegister(MemberVo mvo);

	MemberVo findMember(String userid, String password);

	List<MemberVo> getWaitList();

	MemberVo getWaitMember(String id);

	String getCompanyname(int companyidx);

	Map<String, Object> getCompanyList(Map<String, Object> searchMap);

}
