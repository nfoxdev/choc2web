package com.nainfox.service;

import java.util.List;
import java.util.Map;

import com.nainfox.vo.BrandVo;

public interface BrandService {

	Map<String, Object> brandSearchPageList(Map<String, Object> brandMap);

	List<BrandVo> getMyBrandList(int company_id);

	BrandVo getOneBrand(String brand_id);


}
