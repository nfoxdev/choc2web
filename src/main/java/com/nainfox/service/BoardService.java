package com.nainfox.service;

import java.io.IOException;
import java.util.Map;

import com.google.zxing.WriterException;
import com.nainfox.vo.BoardVo;

public interface BoardService {

	Map<String, Object> boardSearchPageList(Map<String, Object> boardMap);

	BoardVo getView(int boardIdx);

	void modify(BoardVo bvo);

	void deleteView(int idx);

	void add(BoardVo bvo) throws WriterException, IOException;

	Map<String, Object> noticeSearchPageList(Map<String, Object> searchMap);

}
