package com.nainfox.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nainfox.dao.MemberDao;
import com.nainfox.encryptor.SHA256Encryptor;
import com.nainfox.vo.CompanyVo;
import com.nainfox.vo.MemberVo;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
public class MemberServiceImpl implements MemberService{

	@Setter
	private MemberDao memberDao;
	@Setter
	private MailService mailService;
	
	//일반 멤버
	
	@Override
	@Transactional
	public void memberSignup(MemberVo memberVo) throws SQLException{
		String rawPassword = memberVo.getPassword();
		if(rawPassword.length() != 64) {
			MemberVo encryptedVo = passwordEncrypt(memberVo);
			memberDao.memberSignup(encryptedVo);
			memberDao.addDetail(encryptedVo);
			memberDao.addAuth(encryptedVo);
		}else {
			memberDao.memberSignup(memberVo);
			memberDao.addDetail(memberVo);
			memberDao.addAuth(memberVo);
		}
	}
	
	//기업 승인 대기 페이지
	
	@Override
	public void companyWaitAdd(MemberVo mvo) {
		MemberVo encryptedVo = passwordEncrypt(mvo);
		memberDao.companyWaitAdd(encryptedVo);
	}
	
	private MemberVo passwordEncrypt(MemberVo mvo) {
		String rawPassword = mvo.getPassword();
		String encryptedPassword = SHA256Encryptor.shaEncrypt(rawPassword);
		mvo.setPassword(encryptedPassword);
		return mvo;
	}
	
	//아이디 중복확인
	@Override
	public String idCheck(String userid) throws SQLException{
		int result = memberDao.idCheck(userid);
		int waitResult = memberDao.waitIdCheck(userid);
		if(result + waitResult != 0) {
			return "dual";
		}else {
			return "use";
		}
	}
	//이메일 중복확인
	@Override
	public String emailCheck(String email) throws SQLException{
		int result = memberDao.emailCheck(email);
		int waitResult = memberDao.waitEmailCheck(email);
		if(result + waitResult != 0) {
			return "dual";
		}else {
			return "use";
		}
	}

	//기업 가입 승인
	@Transactional
	public void acceptCompanyRegister(MemberVo mvo) throws SQLException {
		//중복 기업이 있는지 체크
		//기업테이블에 기업 추가
		//기업 테이블의 companyid 리턴
		//mvo에 companyid 설정
		//회원가입 승인
		//메일 발송
		//승인 대기 목록에서 삭제
		
		if(!checkCompanyDual(mvo.getCompanyname())) {
			memberDao.addCompany(mvo);
			int company_id = memberDao.getCompanyidx(mvo.getCompanyname());
			mvo.setCompany_id(company_id);
			memberSignup(mvo);
			mailService.acceptEmailSend(mvo);
			memberDao.deleteWaitRegister(mvo.getUserid());
		}else{
			int company_id = memberDao.getCompanyidx(mvo.getCompanyname());
			mvo.setCompany_id(company_id);
			memberSignup(mvo);
			mailService.acceptEmailSend(mvo);
			memberDao.deleteWaitRegister(mvo.getUserid());
		}
		
	}
	
	//기업명 중복확인
	private boolean checkCompanyDual(String companyName) {
		int result = memberDao.checkCompanyDual(companyName);
		log.info(result);
		if(result == 0) {
			return false;
		}else {
			return true;
		}
	}

	//기업 승인거절
	public void denyCompanyRegister(MemberVo mvo) {
		mailService.denyEmailSend(mvo);
		memberDao.deleteWaitRegister(mvo.getUserid());
	}
	
	//로그인
	@Override
	public MemberVo findMember(String userid, String password) {
		Map<String, String> loginData = new HashMap<>();
		loginData.put("userid", userid);
		loginData.put("password", SHA256Encryptor.shaEncrypt(password));
		MemberVo findMember = memberDao.findMember(loginData);
		return findMember;
	}
	
	//승인 대기 기업 리스트 리턴
	
	@Override
	public List<MemberVo> getWaitList() {
		return memberDao.getWaitRegisterList();
	}
	
	//승인 대기 기업회원 리스트
	@Override
	public MemberVo getWaitMember(String id) {
		return memberDao.getWaitMember(id);
	}
	
	//기업 이름 리턴
	@Override
	public String getCompanyname(int companyidx) {
		return memberDao.getCompanyname(companyidx);
	}

	@Override
	public Map<String, Object> getCompanyList(Map<String, Object> searchMap) {
		int maxCountOfOneList = 20;
		int totalCount = memberDao.companyTotalCount();
		System.out.println(totalCount + "총 기업수");
		int page = (int)(searchMap.get("page"));
		int startRownum = (page - 1) * maxCountOfOneList + 1;
		int endRownum = startRownum + (maxCountOfOneList - 1);
		
		searchMap.put("total", totalCount);
		searchMap.put("start", startRownum);
		searchMap.put("end", endRownum);
		
		List<CompanyVo> companyList = memberDao.companySearchPage(searchMap);
		searchMap.put("companyList", companyList);
		int totalPage = 0;
		
		if(totalCount % maxCountOfOneList == 0) {
			totalPage = totalCount / maxCountOfOneList;
		}else {
			totalPage = (totalCount / maxCountOfOneList) + 1;
		}
		System.out.println(totalPage + "총페이지");
		searchMap.put("totalPage", totalPage);
		return searchMap;
	}
	
	
}
