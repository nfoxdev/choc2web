package com.nainfox.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.nainfox.dao.BrandDao;
import com.nainfox.vo.BrandVo;

import lombok.Setter;

@Service
public class BrandServiceImpl implements BrandService{

	@Setter
	private BrandDao brandDao;
	
	private static final int maxCountOfOneList = 10;
	
	@Override
	public Map<String, Object> brandSearchPageList(Map<String, Object> searchMap) {
		int totalCount = brandDao.totalCount(searchMap);
		int page = (int)(searchMap.get("page"));
		int startRownum = (page - 1) * maxCountOfOneList + 1;
		int endRownum = startRownum + (maxCountOfOneList - 1);
		
		searchMap.put("total", totalCount);
		searchMap.put("start", startRownum);
		searchMap.put("end", endRownum);
		
		List<BrandVo> brandList = brandDao.brandSearchPage(searchMap);
		searchMap.put("brandList", brandList);
		int totalPage = 0;
		
		if(totalCount % maxCountOfOneList == 0) {
			totalPage = totalCount / maxCountOfOneList;
		}else {
			totalPage = (totalCount / maxCountOfOneList) + 1;
		}
		
		searchMap.put("totalPage", totalPage);
		return searchMap;
	}

	@Override
	public List<BrandVo> getMyBrandList(int company_id) {
		return brandDao.getMyBrandList(company_id);
	}
	
	@Override
	public BrandVo getOneBrand(String brand_id) {
		return brandDao.getOne(brand_id);
	}
	
	
	
	
	
	
}
