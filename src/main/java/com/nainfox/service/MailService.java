package com.nainfox.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;

import com.nainfox.dao.MemberDao;
import com.nainfox.vo.MemberVo;

import lombok.Setter;

@Service
public class MailService implements ServletContextAware{

	@Setter
	private JavaMailSender javaMailSender;
	@Setter
	private MemberDao memberDao;
	@Setter
	private ServletContext servletContext;
	
	public void sendQRMail(String userid, String valkey) {
		MemberVo mvo = memberDao.getMemberFromId(userid);
		
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(msg, true, "UTF-8");
			helper.setFrom("register@ninefox.com");
			helper.setTo(mvo.getEmail());
			helper.setSubject("게시글이 등록됐습니다.");
			String contents = "<p>아래 QR코드를 스캔하세요</p><br /><img src=\"cid:"+valkey+".png\"/>"; 
			helper.setText(contents, true); 
			FileSystemResource file = new FileSystemResource(new File("/Users/nfoxdev04/Documents/qrcodes/"+valkey+".png")); 
			helper.addInline(valkey+".png", file);
			javaMailSender.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}
	
	public void denyEmailSend(MemberVo mvo) {

		MimeMessage message = javaMailSender.createMimeMessage();
		try {

			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			//cid 속성 이용할 이미지 파일 등
			//FileSystemResource file = new FileSystemResource(new File("/Users/nfoxdev04/Downloads/choc2.png"));
			
			String mailPath = servletContext.getRealPath("/WEB-INF/templates/denyMail.html");
			String resourcePath = servletContext.getRealPath("/resources/images");
			File choc2Img = new File(resourcePath+"/choc2.png");
			File choc3Img = new File(resourcePath+"/choc3.png");
			
			String content = "";
		    try {
		        BufferedReader in = new BufferedReader(new FileReader(mailPath));
		        String str;
		        while ((str = in.readLine()) != null) {
		            content += str;
		        }
		        in.close();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
			content = content.replace("@username@", mvo.getUsername());
			helper.setTo(mvo.getEmail());
			helper.setText(content, true);
			helper.setSubject("CHOC2 통합관리 서비스 가입이 거절되었습니다.");
			helper.setFrom("register@choc2.com");
			helper.addInline("choc2Img", choc2Img);
			helper.addInline("choc3Img", choc3Img);
			javaMailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
	
	public void acceptEmailSend(MemberVo mvo) {

		MimeMessage message = javaMailSender.createMimeMessage();
		try {

			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			//cid 속성 이용할 이미지 파일 등
			//FileSystemResource file = new FileSystemResource(new File("/Users/nfoxdev04/Downloads/choc2.png"));
			
			String mailPath = servletContext.getRealPath("/WEB-INF/templates/regMail.html");
			String resourcePath = servletContext.getRealPath("/resources/images");
			File choc2Img = new File(resourcePath+"/choc2.png");
			File choc3Img = new File(resourcePath+"/choc3.png");
			
			String content = "";
		    try {
		        BufferedReader in = new BufferedReader(new FileReader(mailPath));
		        String str;
		        while ((str = in.readLine()) != null) {
		            content += str;
		        }
		        in.close();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
			content = content.replace("@username@", mvo.getUsername());
			helper.setTo(mvo.getEmail());
			helper.setText(content, true);
			helper.setSubject("CHOC2 통합관리 서비스 가입이 승인되었습니다.");
			helper.setFrom("register@choc2.com");
			helper.addInline("choc2Img", choc2Img);
			helper.addInline("choc3Img", choc3Img);
			javaMailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
}
