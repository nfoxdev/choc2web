package com.nainfox.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.WriterException;
import com.nainfox.dao.BoardDao;
import com.nainfox.vo.BoardVo;

import lombok.Setter;


@Service
public class BoardServiceImpl implements BoardService{
	
	@Setter
	private BoardDao boardDao;
	@Setter
	private QrService qrService;
	@Setter
	private MemberService memberService;
	@Setter
	private MailService mailService;
	
	private static final int maxCountOfOneList = 10;

	@Override
	public Map<String, Object> boardSearchPageList(Map<String, Object> searchMap) {
		int totalCount = boardDao.totalCount(searchMap);
		int page = (int)(searchMap.get("page"));
		int startRownum = (page - 1) * maxCountOfOneList + 1;
		int endRownum = startRownum + (maxCountOfOneList - 1);
		
		searchMap.put("total", totalCount);
		searchMap.put("start", startRownum);
		searchMap.put("end", endRownum);
		
		List<BoardVo> boardList = boardDao.boardSearchPage(searchMap);
		compareDay(boardList);
		searchMap.put("boardList", boardList);
		int totalPage = 0;
		
		if(totalCount % maxCountOfOneList == 0) {
			totalPage = totalCount / maxCountOfOneList;
		}else {
			totalPage = (totalCount / maxCountOfOneList) + 1;
		}
		
		searchMap.put("totalPage", totalPage);
		return searchMap;
	}
	
	@Override
	public Map<String, Object> noticeSearchPageList(Map<String, Object> searchMap) {
		int totalCount = boardDao.totalCount(searchMap);
		int page = (int)(searchMap.get("page"));
		int startRownum = (page - 1) * maxCountOfOneList + 1;
		int endRownum = startRownum + (maxCountOfOneList - 1);
		
		searchMap.put("total", totalCount);
		searchMap.put("start", startRownum);
		searchMap.put("end", endRownum);
		
		List<BoardVo> boardList = boardDao.noticeSearchPage(searchMap);
		compareDay(boardList);
		searchMap.put("boardList", boardList);
		int totalPage = 0;
		
		if(totalCount % maxCountOfOneList == 0) {
			totalPage = totalCount / maxCountOfOneList;
		}else {
			totalPage = (totalCount / maxCountOfOneList) + 1;
		}
		
		searchMap.put("totalPage", totalPage);
		return searchMap;
	}
	
	private List<BoardVo> compareDay(List<BoardVo> boardList) {
		Calendar today = Calendar.getInstance();
		Integer todayYear = today.get(Calendar.YEAR);
		Integer todayMonth = today.get(Calendar.MONTH) + 1;
		Integer todayDate = today.get(Calendar.DATE);
		
		String strMonth = String.valueOf(todayMonth);
		if(todayMonth < 10) {
			strMonth = "0" +todayMonth;
		}
		String toDay = todayYear+"-"+strMonth+"-"+todayDate;
		
		for(int i = 0; i < boardList.size(); i++) {
			if(toDay.equals(boardList.get(i).getRegdate().substring(0, 10))) {
				boardList.get(i).setRegdate(boardList.get(i).getRegdate().substring(11, 16));
			}else {
				boardList.get(i).setRegdate(boardList.get(i).getRegdate().replaceAll("-", ".").substring(0, 10));
			}
		}
		return boardList;
	}

	@Override
	public BoardVo getView(int boardIdx) {
		return boardDao.getView(boardIdx);
	}

	@Override
	public void modify(BoardVo bvo) {
		boardDao.updatePost(bvo);
	}

	@Override
	public void deleteView(int idx) {
		boardDao.deletePost(idx);
	}

	@Override
	@Transactional
	public void add(BoardVo bvo) throws WriterException, IOException {
		boardDao.addPost(bvo);
		int addedPostIdx = bvo.getIdx();
		Integer addedCompanyIdx = bvo.getCompany_id();
		if(addedCompanyIdx != 0 || addedCompanyIdx == null) {
			qrService.addQRCode(addedPostIdx+"", "/company/"+addedCompanyIdx+"/board/view?id="+addedPostIdx, null);
		}else {
			qrService.addQRCode(addedPostIdx+"", "/board/view?id="+addedPostIdx, null);
		}
		System.out.println("추가된 후 idx : " + addedPostIdx);
		System.out.println("userid : " + bvo.getUserid());
		//파라미터값 (userid, valkey(=파일명))
		//mailService.sendQRMail(bvo.getUserid(), addedPostIdx+"");
	}
	
	
	
	

	
	
}
