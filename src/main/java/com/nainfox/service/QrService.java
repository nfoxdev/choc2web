package com.nainfox.service;

import java.io.IOException;
import java.util.List;

import com.google.zxing.WriterException;
import com.nainfox.vo.QrdataVo;

public interface QrService {

	List<QrdataVo> findAll();

	QrdataVo findOne(int id);

	void addQRCode(String vkey, String val, String quantity) throws WriterException, IOException;

	String getValue(String valkey);

	
	
}
