package com.nainfox.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class IFormService {
	
	public Map<String, String> getPhoneMap(){
		Map<String, String> phoneMap = new LinkedHashMap<>();
		phoneMap.put("010", "010");
		phoneMap.put("011", "011");
		phoneMap.put("016", "016");
		phoneMap.put("017", "017");
		phoneMap.put("018", "018");
		phoneMap.put("019", "019");
		return phoneMap;
	}
	
	
}
