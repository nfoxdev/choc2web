package com.nainfox.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.nainfox.vo.MemberVo;

import lombok.extern.log4j.Log4j;

@Component
@Aspect
@Log4j
public class LoggerAspect {

	@Around("execution(* com.nainfox.controller.*Controller.*(..))")
	public Object logprint(ProceedingJoinPoint jp) throws Throwable {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		long start = System.currentTimeMillis();
		Object result = jp.proceed();
		long end = System.currentTimeMillis();
		HttpSession session = req.getSession();
		MemberVo member = (MemberVo)(session.getAttribute("loginMember"));
		String memberId = null;
		if(member != null) {
			memberId = member.getUserid();
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append("\nID : " + memberId + "\n");
		sb.append("요청 경로 : " + req.getRequestURI() + "\n");
		sb.append("이전 경로 : " + req.getHeader("referer") + "\n");
		sb.append("처리 클래스 : " + jp.getSignature().getDeclaringTypeName() + "\n");
		sb.append("처리 메서드 : " + jp.getSignature().getName() + "\n");
		sb.append("IP : " + req.getRemoteAddr() + "\n");
		sb.append("소요 시간 : " + (end - start) * 0.001 + "s");
		
		log.warn(sb.toString());
		

	/*	
		
		 전달되는 모든 파라미터들을 Object의 배열로 가져온다. 
		 log.info("1:" + Arrays.toString(jp.getArgs()));
		    
		 해당 Advice의 타입을 알아낸다. 
		log.info("2:" + jp.getKind());
		    
		 실행하는 대상 객체의 메소드에 대한 정보를 알아낼 때 사용 
		log.info("3:" + jp.getSignature().getName());
		    
		 target 객체를 알아낼 때 사용 
		log.info("4:" + jp.getTarget().toString());
		    
		 Advice를 행하는 객체를 알아낼 때 사용 
		log.info("5:" + jp.getThis().toString());*/
		return result;
	}
	
	
	
}
