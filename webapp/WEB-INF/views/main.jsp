<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon" href="assets/images/choc2-122x45.png"
	type="image/x-icon">
<meta name="description" content="Website Creator Description">
<title>Page 1</title>
<link rel="stylesheet"
	href="assets/web/assets/mobirise-icons/mobirise-icons.css">
<link rel="stylesheet" href="assets/tether/tether.min.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/bootstrap/css/bootstrap-grid.min.css">
<link rel="stylesheet"
	href="assets/bootstrap/css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="assets/socicon/css/styles.css">
<link rel="stylesheet" href="assets/dropdown/css/style.css">
<link rel="stylesheet" href="assets/theme/css/style.css">
<link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css"
	type="text/css">
</head>
<body>
	<section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-3">

		<nav
			class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">
			<button class="navbar-toggler navbar-toggler-right" type="button"
				data-toggle="collapse" data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<div class="hamburger">
					<span></span> <span></span> <span></span> <span></span>
				</div>
			</button>
			<div class="menu-logo">
				<div class="navbar-brand">
					<span class="navbar-logo"> <a href="/"> <img
							src="assets/images/choc2-122x45.png" style="height: 3.8rem;">
					</a>
					</span>

				</div>
			</div>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
					<li class="nav-item"><a
						class="nav-link link text-secondary display-4" href="/brand">
							<span class="mbr-iconfont mbr-iconfont-btn"></span> 뷰티 브랜드
					</a></li>
				</ul>
				<ul class="navbar-nav nav-dropdown nav-right"
					data-app-modern-menu="true">
					<c:if test="${loginMember.auth.role == 'company' }">
						<li class="nav-item"><a
							class="nav-link link text-secondary display-4"
							href="/company/${sessionScope.loginMember.company_id }"> <span
								class="mbr-iconfont mbr-iconfont-btn"></span> 기업 페이지
						</a></li>
					</c:if>
					<c:if test="${loginMember.auth.role == 'admin'}">
						<li class="nav-item"><a
							class="nav-link link text-secondary display-4" href="/admin">
								<span class="mbr-iconfont mbr-iconfont-btn"></span> 관리자 페이지
						</a></li>
					</c:if>
					<c:if test="${empty sessionScope.loginMember }">
						<li class="nav-item"><a
							class="nav-link link text-secondary display-4" href="/signup"><span
								class="mbr-iconfont mbr-iconfont-btn"></span> 회원가입 </a></li>
						<li class="nav-item"><a
							class="nav-link link text-secondary display-4" href="/login"><span
								class="mbr-iconfont mbr-iconfont-btn"></span> 로그인 </a></li>
					</c:if>
					<c:if test="${!empty sessionScope.loginMember }">
						<li class="nav-item"><a
							class="nav-link link text-secondary display-4" href="/logout"><span
								class="mbr-iconfont mbr-iconfont-btn"></span> 로그아웃 </a></li>
					</c:if>
				</ul>

			</div>
		</nav>
	</section>

	<section class="engine">
		<a href="https://mobirise.info/v">html templates</a>
	</section>
	<section class="header1 cid-ra0gxf7VYm mbr-parallax-background"
		id="header1-7">



		<div class="mbr-overlay"
			style="opacity: 0.2; background-color: rgb(0, 0, 0);"></div>

		<div class="container">
			<div class="row justify-content-md-center">
				<div class="mbr-white col-md-10">
					<div class="mbr-section-btn align-center">
						<a class="btn btn-md btn-secondary display-7" href="/404">젠킨스로
							찾기</a> <a class="btn btn-md btn-secondary display-7" href="/brand">브랜드로
							찾기</a> <a class="btn btn-md btn-secondary display-7" href="/404">피부타입으로
							찾기</a>
					</div>
				</div>
			</div>
		</div>

	</section>

	<section class="features16 cid-ra0swWrkYW" id="features16-b">



		<div class="container align-center">
			<h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">
				HOT ITEM</h2>

			<div class="row media-row">
				<c:forEach var="cos" items="${cosmeticList }">
					<div class="team-item col-lg-3 col-md-6">
						<a href="/cosmetic/${cos.id}">
							<div class="item-image">
							<img src="${cos.img_path }"
								style="width: 120px; height: 120px; margin: 0; padding: 0;" />
							</div>
						</a>
						<div class="item-caption py-3">
							<div class="item-name px-2">
								<p class="mbr-fonts-style display-5"><a href="/cosmetic/${cos.id }">${cos.kor_name }</a></p>
							</div>
							<div class="item-role px-2">
								<p>${cos.brand.kor_name }</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<!--  -->

			<div class="mbr-section-btn align-right">
				<a class="btn btn-md btn-secondary display-7" href="/brand">->
					더보기</a>
			</div>
		</div>
	</section>

	<section class="header1 cid-ra0npTHrAb mbr-parallax-background"
		id="header1-9">





		<div class="container">
			<div class="row justify-content-md-center">
				<div class="mbr-white col-md-10">
					<h1
						class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-1">
						피부 상태 분석</h1>


					<div class="mbr-section-btn align-center">
						<a class="btn btn-md btn-white-outline display-4" href="/404">
							<span class="mbr-iconfont mbri-idea"></span>피부 측정 & 분석
						</a>
					</div>
				</div>
			</div>
		</div>

	</section>
	<section class="cid-ra0MNZUqjG mbr-reveal" id="footer1-i">
		<div class="container">
			<div class="media-container-row content text-white">
				<div class="col-12 col-md-3">
					<div class="media-wrap">
						<a href="/"> <img src="assets/images/choc2-180x67.png">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-3 mbr-fonts-style display-7">
					<h5 class="pb-3">Address</h5>
					<p class="mbr-text">
						서울시 구로구 디지털로 32길 30 <br>코오롱디지털타워빌란트 801-3호 <br />나인폭스(주)
					</p>
				</div>
				<div class="col-12 col-md-3 mbr-fonts-style display-7">
					<h5 class="pb-3">Contacts</h5>
					<p class="mbr-text">
						CEO: 임경태 <br />Email: ninefox901@gmail.com <br>Phone: +82
						70-7592-2417
					</p>
				</div>
			</div>
			<div class="footer-lower">
				<div class="media-container-row">
					<div class="col-sm-12">
						<hr>
					</div>
				</div>
				<div class="media-container-row mbr-white">
					<div class="col-sm-6 copyright">
						<p class="mbr-text mbr-fonts-style display-7">© Copyright 2018
							나인폭스(주) - All Rights Reserved</p>
					</div>
					<div class="col-md-6"></div>
				</div>
			</div>
		</div>
	</section>

	<script src="assets/web/assets/jquery/jquery.min.js"></script>
	<script src="assets/popper/popper.min.js"></script>
	<script src="assets/tether/tether.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/dropdown/js/script.min.js"></script>
	<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
	<script src="assets/parallax/jarallax.min.js"></script>
	<script src="assets/smoothscroll/smooth-scroll.js"></script>
	<script src="assets/theme/js/script.js"></script>


</body>
</html>