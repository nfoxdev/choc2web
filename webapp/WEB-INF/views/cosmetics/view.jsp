<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style>
.star-rating { width:205px; }   /* 별이미지크기 */
.star-rating,.star-rating span { display:inline-block; height:39px; overflow:hidden; background:url(/images/star.png)no-repeat; } /* 두개의 span에 동시에 주는 효과들 */
.star-rating span{ background-position:left bottom; line-height:0; vertical-align:top; } /* 안쪽 span에만 적용 */


</style>
<body>
<jsp:include page="../include/nav.jsp" />
<div class="header">
	
</div>

<div class="container">

	<!-- 기초정보 -->
	<div class="row text-center">
		<img src="${cosmeticVo.img_path }"  />
	</div>
	<div class="row text-center">
		${cosmeticVo.brand.kor_name }(${cosmeticVo.brand.eng_name }) <span class="label label-primary"><a href="/brand/view?id=${cosmeticVo.brand.id }" style="color: white;">이 브랜드의 다른 제품도 볼까요?</a></span>
	</div>
	<div class="row text-center">
		<p style="font-size: 12pt; font-weight: bold;">${cosmeticVo.kor_name }</p>
	</div>
	<div class="row">
		<span style="font-size: 12pt; font-weight: bold;">평점</span>
		<span class="star-rating">
			<span style="width:90%"></span>
		</span>
	</div>
	<div class="row">
		<span style="font-size: 12pt; font-weight: bold;">용량 및 가격</span> <span style="font-size: 12pt;">100ml / <b><fmt:formatNumber value="${cosmeticVo.price}" pattern="#,###" /></b>원</span>
	</div>
	<div class="row">
		<span style="font-size: 12pt; font-weight: bold;">카테고리</span>
		<span style="font-size: 12pt;">에센스</span>
	</div>
	
	<!-- 성분분석 -->
	<div class="row">
		<p>성분 분석</p>
		<img src="/images/ingredummy.png" alt="" />
	</div>
	<!-- 제품정보 -->
	<div class="row">
		<p style="font-size: 12pt; font-weight: bold;">제품 상세 정보</p>
		<p style="font-size: 12pt;">이 제품은 녹차 성분이 함유되어 녹차의 은은한 향이 납니다. 또한 녹차향으로 인해 식욕을 절제해주고 그로인해 살이빠지고 살이빠지면 외형이 살아나고 그 결과 삶의 질이 올라가는 기적의 화장품입니다.</p>
	</div>
	
	<!-- 리뷰 -->
		
	
</div>


</body>
</html>