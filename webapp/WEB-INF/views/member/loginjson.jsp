<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/login.css">
<body>
	<h1>로그인</h1>
	<form action="/loginjson" method="post">
		<input type="hidden" name="prevURL" value="<%=request.getHeader("referer") %>" />
	  <ul>
	      <li class="text">ID</li>
	      <li><input type="text" name="userid"></li>
	      <li class="text">PW</li>
	      <li><input type="password" name="password"></li>
	      <li><input type="submit"></li>
	  </ul>
	</form>
</body>
</html>