<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/member.css">
<script>
	function idCheck(){
		var $id = $("#userid").val();
		$.ajax({
			url : "/signup/idCheck",
			data : {userid : $id},
			type : "post",
			success : function(data){
				if(data == "dual"){
					alert("중복된 아이디입니다.");
				}else if(data == "use"){
					alert("사용 가능한 아이디입니다.");
				}else{
					alert("서버 오류입니다. 잠시 후 다시 시도하세요.");
				}
			}	
		});
		
	}
</script>
<body>
	<h1>일반회원 가입</h1>
	<form:form modelAttribute="memberVo" action="/signup/member" method="post">
	  <ul>
	      <li class="text">아이디</li>
	      <li>
	      	<form:input type="text" path="userid" id="userid" /><form:errors path="userid" class="errors"></form:errors>
	      	<input type="button" value="중복 확인" onclick="idCheck();">
	      </li>
	      <li class="text">패스워드</li>
	      <li><form:input type="password" path="password" /><form:errors path="password" class="errors"></form:errors></li>
	      <li class="text">이름</li>
	      <li><form:input type="text" path="username" /><form:errors path="username" class="errors"></form:errors></li>
	      <li class="text">전화번호</li>
	      <li>
	      	<form:select path="phone1" items="${phoneMap }" />-
	      	<form:errors path="phone1" class="errors"></form:errors>
	      	<form:input path ="phone2" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" maxlength="4"/>-
	      	<form:errors path="phone2" class="errors"></form:errors>
	      	<form:input path ="phone3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" maxlength="4"/>
	      	<form:errors path="phone3" class="errors"></form:errors>
	      	<form:errors path="phone" class="errors"></form:errors>
	      </li>
	      <li class="text">이메일</li>
	      <li>
	      	<form:input type="text" path="email1" maxlength="15"/>@
	      	<form:errors path="email1" class="errors"></form:errors>
	      	<form:input type="text" path="email2" maxlength="15" />
	      	<form:errors path="email2" class="errors"></form:errors>
	      	<form:errors path="email" class="errors"></form:errors>
	      </li>
	      <li><input type="submit"><a class="btn btn-default" href="/signup/member">취소</a></li>
	  </ul>
	</form:form>
</body>
</html>