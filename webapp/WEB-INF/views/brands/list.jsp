<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp" />
<script type="text/javascript" src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript" src="/js/list.js"></script>
<script type="text/javascript" src="/js/flowtype.js"></script>
<script type="text/javascript" src="/js/nav.js"></script>
<link rel="stylesheet" href="/css/common.css">
<link rel="stylesheet" href="/css/list.css">
<body>
	<jsp:include page="../include/nav.jsp" />
	<h1>::뷰티 브랜드 리스트::</h1>
	<div class="searchList">
		<div class="form-group">
			<input type="text" id="keyword" name="keyword" onkeypress="if(event.keyCode==13) {repaging(event, 1);};"
				class="form-control" placeholder="찾고 싶은 브랜드를 입력하세요." />
			<div class="btn_search">
				<button type="button" onclick='repaging(event, 1);' 
					class="btn btn_search2">검색</button>
			</div>
		</div>
	</div>
	<table id="listTable">
		<c:if test="${empty brandMap.brandList }">
			<tr>
				<td colspan="5">조회한 결과가 없습니다.</td>
			</tr>
		</c:if>
		<c:forEach var="brand" items="${brandMap.brandList }">
			<tr>
				<td><img src="${brand.img_path }" alt="존재하지 않는 이미지.png" /></td>
				<td><a href="${viewURI }?id=${brand.id }">${brand.kor_name }(${brand.eng_name })</a></td>
			</tr>
		</c:forEach>
	</table>
	<div id="page_selection"></div>
	    <script>
	        // init bootpag
	        var myboot = $('#page_selection').bootpag({
			    total: ${brandMap.totalPage},
			    page: 1,
			    maxVisible: 5,
			    leaps: true,
			    firstLastUse: true,
			    first: '←',
			    last: '→',
			    wrapClass: 'pagination',
			    activeClass: 'active',
			    disabledClass: 'disabled',
			    nextClass: 'next',
			    prevClass: 'prev',
			    lastClass: 'last',
			    firstClass: 'first'
			}).on("page", function(event, num){
				var $text = $('#keyword').val();
			    $.ajax({
			    	url : "/brand/brandPaging",
			    	type : "POST",
			    	data : {page : num, keyword : $text},
			    	success : function(data){
			    		//총 페이지의 수
			    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
		    			myboot.bootpag({total: data.totalPage, page : num})
			    		 //데이터 넣기
			    		 $('#listTable>tbody').empty();
							if(data.brandList.length == 0){
								$('#listTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
							}
							for(var i of data.brandList){
								$('#listTable>tbody').append('<tr><td>'+'<img src="'+i.img_path+'" alt="존재하지 않는 이미지.png" /></td><td>'+
										'<a href="/brand/view/?id='+i.id+'">'+i.kor_name+'('+i.eng_name+')</a>'+'</td></tr>');	
							}
			    	}
			    })
			})

	        var repaging = function(event, num){
				var $text = $('#keyword').val();
			    $.ajax({
			    	url : "/brand/brandPaging",
			    	type : "POST",
			    	data : {page : num, keyword : $text},
			    	success : function(data){
			    		//총 페이지의 수
			    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
			    		myboot.bootpag({total: data.totalPage, page : num})
			    		 //데이터 넣기
			    		 $('#listTable>tbody').empty();
							if(data.brandList.length == 0){
								$('#listTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
							}
							for(var i of data.brandList){
								$('#listTable>tbody').append('<tr><td>'+'<img src="'+i.img_path+'" alt="존재하지 않는 이미지.png" /></td><td>'+
										'<a href="/brand/view/?id='+i.id+'">'+i.kor_name+'('+i.eng_name+')</a>'+'</td></tr>');
							}
			    	}
			    })
			}
	        
	    </script>
	
    <script type="text/javascript">
      $('body').flowtype();
  	</script>
</body>
</html>