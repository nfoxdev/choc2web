<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">CHOC2</a>
    </div>
     <ul class="nav navbar-nav">
     	<li><a href="/brand">뷰티 브랜드</a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
     <c:if test="${loginMember.auth.role == 'company' }">
     	<li><a href="/company/${loginMember.company_id }">기업 게시판</a></li>
     </c:if>
     <c:if test="${loginMember.auth.role == 'admin'}">
     	<li><a href="/admin">관리자</a></li>
     </c:if>
     <c:if test="${empty sessionScope.loginMember }">
     	<li><a href="/signup">회원가입</a></li>
		<li><a href="/login">로그인</a></li>     
     </c:if>
     <c:if test="${!empty sessionScope.loginMember }">
     	<li><a href="/logout">로그아웃</a></li>
     </c:if>
    </ul>
  </div>
</nav>