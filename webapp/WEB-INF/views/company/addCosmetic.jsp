<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>:::choc2:::</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style>
.required{
	color:red;
	font-size: 120%;
}
.errors{
	color:#B40404;
	font-weight: bold;
}
</style>
<body>
<jsp:include page="../include/nav.jsp" />
<div class="header">
	<h4 class="text-center">${sessionScope.brand.kor_name } 브랜드 상품 추가/수정</h4><br />
</div>
<div class="container">
	<div class="row">
		<form:form action="${actionURI}" modelAttribute="cosmeticVo" method="post" class="form-horizontal">
			<div class="form-group">
				<label for="kor_name" class="control-label col-sm-2"><span class="required">*</span>상품명(국문)</label>
				<div class="col-sm-6">
					<form:input type="text" class="form-control" path="kor_name" placeholder="상품명을 적어주세요."/>
					<form:errors path="kor_name" class="errors" value=""/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="eng_name" class="control-label col-sm-2"><span class="required">*</span>상품명(영문)</label>
				<div class="col-sm-6">
					<form:input type="text" class="form-control" path="eng_name" placeholder="상품의 영문명을 적어주세요."/>
					<form:errors path="eng_name" class="errors" value=""/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="kor_name" class="control-label col-sm-2"><span class="required">*</span>상품 이미지</label>
				<div class="col-sm-6">
					<form:input type="text" class="form-control" path="img_path" placeholder="상품의 이미지 링크를 적어주세요."/>
					<form:errors path="img_path" class="errors" value=""/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="price" class="control-label col-sm-2"><span class="required">*</span>상품 가격</label>
				<div class="col-sm-6">
					<form:input type="text" class="form-control" path="price" placeholder="상품의 가격을 숫자로만 적어주세요." value=""/>
					<form:errors path="price" class="errors" value=""/>
					
				</div>
			</div>
			
			<div class="form-group">
				<label for="kor_name" class="control-label col-sm-2">제품 공식 페이지 링크</label>
				<div class="col-sm-6">
					<form:input type="text" class="form-control" path="link" placeholder="상품의 공식 페이지 링크를 적어주세요."/>
					<form:errors path="link" class="errors" value=""/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="functional" class="control-label col-sm-2"><span class="required">*</span>기능성 유무</label>
				<div class="col-sm-6">
					<div class="radio-inline">
						<form:radiobutton class="form-control" path="functional" value="1" onclick="viewFuncCheckbox();"/>기능성 있음
					</div>
					<div class="radio-inline">
						<form:radiobutton class="form-control" path="functional" value="0" onclick="closeFunc();" checked="checked"/>기능성 없음
					</div>
					<form:errors path="functional" class="errors"/>
				</div>
			</div>
			
			<div class="form-group funcDiv" style="display: none;">
				<label class="control-label col-sm-2"><span class="required">*</span>기능성 선택</label>
				<div class="col-sm-6">
					<label class="checkbox-inline">
				    	<form:checkbox class="form-control funcBox" path="func_white" value="1"/>미백 효과
				    </label>
				    <label class="checkbox-inline">
						<form:checkbox class="form-control funcBox" path="func_wrinkle" value="1"/>주름 개선
					</label>
					<label class="checkbox-inline">
						<form:checkbox class="form-control funcBox" path="func_uv" value="1"/>자외선 차단
					</label>
				</div>
			</div>
			<div class="row btn-group col-sm-4 col-sm-offset-5">
				<button class="btn btn-success">등록</button>
				<a class="btn btn-danger" href="${cancelURI}">취소</a>
				<button class="btn btn-primary" type="reset">초기화</button>
			</div>
		</form:form>
	</div>
</div>
	<script>
		function viewFuncCheckbox(){
			$(".funcDiv").css("display", "inherit");
		}
		
		function closeFunc(){
			$(".funcBox").prop('checked', false);
			$(".funcDiv").css("display", "none");
		}
	</script>
</body>
</html>