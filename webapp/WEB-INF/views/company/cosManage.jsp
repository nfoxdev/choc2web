<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp" />
<script type="text/javascript" src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript" src="/js/list.js"></script>
<script type="text/javascript" src="/js/flowtype.js"></script>
<script type="text/javascript" src="/js/nav.js"></script>
<link rel="stylesheet" href="/css/common.css">
<link rel="stylesheet" href="/css/list.css">
<script>
	
	function cosDel(kor_name, cosmetic_id){
		if(confirm("정말로 '"+kor_name+"' 제품을 삭제하시겠습니까?")){
			$.ajax({
				url : "/company/${sessionScope.brand.company_id}/brand/${sessionScope.brand.id}/cosmetic/del/"+cosmetic_id,
				type : "post",
				data : {company_id : "${sessionScope.brand.company_id}", bid : "${sessionScope.brand.id}", cosmetic_id : cosmetic_id},
				success : function(data){
					if(data == "success"){
						alert("삭제가 완료됐습니다.");
					}else{
						alert("서버 오류입니다. 잠시 후 다시 시도하세요.");
					}
					location.reload();
				}
			});
		}
	}
</script>
<body>
	<jsp:include page="../include/nav.jsp" />
	<div class="container">
		<h2>:::브랜드 제품 관리:::</h2>
		<div class="row">
			<div class="col-sm-4 col-offset-sm-4">
				<img src="${sessionScope.brand.img_path }"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-offset-sm-4">
				<p>${sessionScope.brand.kor_name} 브랜드</p>
			</div>
		</div>
		<div class="row">
			<div class="searchList">
				<div class="form-group">
					<input type="text" id="keyword" name="keyword" onkeypress="if(event.keyCode==13) {repaging(event, 1);};"
						class="form-control" placeholder="찾고 싶은 제품을 입력하세요." />
					<div class="btn_search">
						<button type="button" onclick='repaging(event, 1);' 
							class="btn btn_search2">검색</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			${sessionScope.brand.kor_name } 브랜드에는 제품이 ${resultMap.totalCount }개 있습니다.  
			<a class="btn btn-primary" type="button" href="/company/${sessionScope.brand.company_id}/brand/${sessionScope.brand.id}/cosmetic/add">상품 추가</a>
			<a class="btn btn-success" href="/company/${sessionScope.brand.company_id}">관리 페이지로 돌아가기</a>
		</div>
		<div class="row">
			<table id="listTable">
				<c:if test="${empty resultMap.cosmeticList }">
					<tr>
						<td colspan="5">조회한 결과가 없습니다.</td>
					</tr>
				</c:if>
				<c:forEach var="cosmetic" items="${resultMap.cosmeticList }">
					<tr>
						<td><img src="${cosmetic.img_path }" alt="" style="max-width:30%; height:auto;" sizes="(min-width: 200px) 50vw, 100vw"/></td>
						<td>
							${cosmetic.brand.kor_name }(${cosmetic.brand.eng_name }) <br />
							<a href="/cosmetic/${cosmetic.id }">${cosmetic.kor_name }(${cosmetic.eng_name })</a>
						</td>
						<td>
							<a class="btn btn-success btn-block"
								href="/company/${sessionScope.brand.company_id}/brand/${sessionScope.brand.id}/cosmetic/mod/${cosmetic.id }">
								수정</a>
							<a class="btn btn-danger btn-block" 
							onclick="cosDel('${cosmetic.kor_name}', '${cosmetic.id}');">삭제</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			<div id="page_selection"></div>
			    <script>
			        // init bootpag
			        var myboot = $('#page_selection').bootpag({
					    total: ${resultMap.totalPage},
					    page: 1,
					    maxVisible: 5,
					    leaps: true,
					    firstLastUse: true,
					    first: '←',
					    last: '→',
					    wrapClass: 'pagination',
					    activeClass: 'active',
					    disabledClass: 'disabled',
					    nextClass: 'next',
					    prevClass: 'prev',
					    lastClass: 'last',
					    firstClass: 'first'
					}).on("page", function(event, num){
						var $text = $('#keyword').val();
						var $brand_id = '${resultMap.brand_id}';
					    $.ajax({
					    	url : "/brand/cosmeticPaging",
					    	type : "POST",
					    	data : {page : num, keyword : $text, brand_id : $brand_id},
					    	success : function(data){
					    		//총 페이지의 수
					    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
				    			myboot.bootpag({total: data.totalPage, page : num})
					    		 //데이터 넣기
					    		 $('#listTable>tbody').empty();
									if(data.cosmeticList.length == 0){
										$('#listTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
									}
									for(var i of data.cosmeticList){
										$('#listTable>tbody').append('<tr><td>'+'<img src="'+i.img_path+'" alt="존재하지 않는 이미지.png" style="max-width:30%; height:auto;"'+ 
												'sizes="(min-width: 200px) 50vw, 100vw"/></td><td>'+i.brand.kor_name+'('+i.brand.eng_name+') <br />'+
														'<a href="/brand/view/?id='+i.id+'">'+i.kor_name+'('+i.eng_name+')</a>'+'</td></tr>');	
									}
					    	}
					    })
					})
		
			        var repaging = function(event, num){
						var $text = $('#keyword').val();
						var $brand_id = '${resultMap.brand_id}';
					    $.ajax({
					    	url : "/brand/cosmeticPaging",
					    	type : "POST",
					    	data : {page : num, keyword : $text, brand_id : $brand_id},
					    	success : function(data){
					    		//총 페이지의 수
					    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
					    		myboot.bootpag({total: data.totalPage, page : num})
					    		 //데이터 넣기
					    		 $('#listTable>tbody').empty();
									if(data.cosmeticList.length == 0){
										$('#listTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
									}
									for(var i of data.cosmeticList){
										$('#listTable>tbody').append('<tr><td>'+'<img src="'+i.img_path+'" alt="존재하지 않는 이미지.png" style="max-width:30%; height:auto;"'+ 
										'sizes="(min-width: 200px) 50vw, 100vw"/></td><td>'+i.brand.kor_name+'('+i.brand.eng_name+') <br />'+
												'<a href="/brand/view/?id='+i.id+'">'+i.kor_name+'('+i.eng_name+')</a>'+'</td></tr>');
									}
					    	}
					    })
					}
			        
			    </script>
		</div>
	</div>
	
    <script type="text/javascript">
      $('body').flowtype();
  	</script>
</body>
</html>