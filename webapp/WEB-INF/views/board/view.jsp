<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/header.jsp" />
<link rel="stylesheet" href="/resources/css/common.css">
<link rel="stylesheet" href="/resources/css/view.css">
<script type="text/javascript" src="/resources/js/view.js"></script>
<script type="text/javascript" src="/resources/js/flowtype.js"></script>
<script type="text/javascript" src="/resources/js/nav.js"></script>
<body>
	<jsp:include page="../include/nav.jsp" />
	
	<table class="board-view table table-bordered">
		<tr>
			<th>제목</th>
			<td>${board.title }</td>
		</tr>
		<tr>
			<th>이름</th>
			<td>${board.userid }</td>
		</tr>
		<tr>
			<th>내용</th>
			<td colspan="2" class="content">
				${board.content }
			</td>
		</tr>
	</table>
	
	<div class="buttons">
		<a class="btn btn_list btn-lg" href="${listURI}">목록</a>
		<a class="btn btn_list btn-lg" href="${modURI}">수정</a>
		<button type="button" style="text-align: right" class="btn btn_list btn-lg" 
		onclick="del();">삭제</button>
		<form action="${listURI }" method="get" name="redirectToList" ></form>
	</div>
	<script type="text/javascript">
      $('body').flowtype();
  	</script>
  	<script>
	  	function del(){
	  		var $url = '${deleteURI}';
	  		var $idx = ${board.idx};
	   	 if(confirm("정말로 게시글을 삭제하시겠습니까?")){
		   		 $.ajax({
		   			 url : $url,
		   			 type : "post",
		   			 data : {idx : $idx},
		   			 success : function(data){
		   				 if(data == "done"){
		   					 alert("게시물을 삭제했습니다.");
		   					 document.forms['redirectToList'].submit();
		   				 }else if(data == "denied"){
		   					 alert("올바른 요청이 아닙니다.");
		   					document.forms['redirectToList'].submit(); 
		   				 }else{
		   					 alert("서버 오류가 발생했습니다. 잠시 후 다시 시도하세요.");
		   					 location.reload();
		   				 }
		   			 }
		   			 
		   		 })
		   	 }
	     }
  	</script> 
</body>
</html>