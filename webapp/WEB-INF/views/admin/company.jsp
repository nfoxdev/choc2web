<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>:::choc2:::</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/sidebar.css">
<link rel="stylesheet" href="/css/loading.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/js/jquery.bootpag.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
	  $('[data-toggle=offcanvas]').click(function() {
	    $('.row-offcanvas').toggleClass('active');
	  });
	});
	
</script>
<body>
	<jsp:include page="/WEB-INF/views/include/nav.jsp" />
	<div class="row row-offcanvas row-offcanvas-left">
		<!-- 좌측 메뉴 사이드 -->
		<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
			role="navigation">
			<div class="sidebar-nav">
				<ul class="nav">
					<li class="1"><a href="/admin">기업회원 관리</a></li>
					<li class="2 active"><a href="/admin/company/list">기업페이지 조회</a></li>
					<li class="3"><a href="/admin/announce">전체 공지</a></li>
					<!-- <li class="nav-divider"></li> -->
				</ul>
			</div>
		</div>
		<div class="col-xs-6 col-sm-9 bodycontainer">
				<table id="companyTable" class="table table-bordered">
					<thead>
						<tr>
							<th>기업</th>
							<th>대표자</th>
							<th>설립일</th>
							<th>주소</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="company" items="${companyMap.companyList }">
						<tr>
							<td><a href="/company/${company.id }">${company.raw_name }</a></td>
							<td>${company.owner }</td>
							<td>${company.permit_date }</td>
							<td>${company.address }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				<div id="page_selection"></div>
				    <script>
				        // init bootpag
				        var myboot = $('#page_selection').bootpag({
						    total: ${companyMap.totalPage},
						    page: 1,
						    maxVisible: 5,
						    leaps: true,
						    firstLastUse: true,
						    first: '←',
						    last: '→',
						    wrapClass: 'pagination',
						    activeClass: 'active',
						    disabledClass: 'disabled',
						    nextClass: 'next',
						    prevClass: 'prev',
						    lastClass: 'last',
						    firstClass: 'first'
						}).on("page", function(event, num){
							var $text = $('#keyword').val();
						    $.ajax({
						    	url : "/admin/companyPaging",
						    	type : "POST",
						    	data : {page : num, keyword : $text},
						    	success : function(data){
						    		//총 페이지의 수
						    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
					    			myboot.bootpag({total: data.totalPage, page : num})
						    		 //데이터 넣기
						    		 $('#companyTable>tbody').empty();
										if(data.companyList.length == 0){
											$('#companyTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
										}
										for(var i of data.companyList){
											$('#companyTable>tbody').append('<tr><td><a href="/company/'+i.id+'">'+i.raw_name+'</a></td><td>'+i.owner+'</td>'+
												'<td>'+i.permit_date+'</td><td>'+i.address+'</td></tr>');	
										}
						    	}
						    })
						})
				        
				        var repaging = function(event, num){
							var $text = $('#keyword').val();
							var $option = $('#optionSelect').val();
						    $.ajax({
						    	url : "/admin/companyPaging",
						    	type : "POST",
						    	data : {page : num, keyword : $text, option : $option},
						    	success : function(data){
						    		//총 페이지의 수
						    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
						    		myboot.bootpag({total: data.totalPage, page : num})
						    		 //데이터 넣기
						    		 $('#companyTable>tbody').empty();
										if(data.companyList.length == 0){
											$('#companyTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
										}
										for(var i of data.companyList){
											$('#companyTable>tbody').append('<tr><td><a href="/company/'+i.id+'">'+i.raw_name+'</a></td><td>'+i.owner+'</td>'+
													'<td>'+i.permit_date+'</td><td>'+i.address+'</td></tr>');
										}
						    	}
						    })
						}
				        
				    </script>
				    <div class="searchList">
					<div class="form-group">
						<input type="text" id="keyword" name="keyword" onkeypress="if(event.keyCode==13) {repaging(event, 1);};"
							class="form-control" placeholder="검색어를 입력하세요." />
						<div class="btn_search">
							<button type="button" onclick='repaging(event, 1);' 
								class="btn btn_search2">검색</button>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>