<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>:::choc2:::</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/sidebar.css">
<link rel="stylesheet" href="/css/loading.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/js/jquery.bootpag.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
	  $('[data-toggle=offcanvas]').click(function() {
	    $('.row-offcanvas').toggleClass('active');
	  });
	  getWaitRegisterList();
	});

	function getWaitRegisterList(){
		$("#waitTable").empty();
		$("#companyTable").hide();
		$("#page_selection").hide();
		$(".searchList").hide();
		$("#waitTable").show();
		$("#waitTable").append("<tr><th>아이디</th><th>이름</th><th>기업명</th><th>연락처</th><th>이메일</th><th>가입 처리</th></tr>");
		$.ajax({
			url : "/admin/waitlist",
			type : "post",
			success : function(data){
				if(data.length == 0){
					$('#waitTable').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
				}else{
					for(var i of data){
						var jsonString = JSON.stringify(i);
						$("#waitTable").append("<tr><td><a href='/admin/eval?id="+i.userid+"'>"+ i.userid +"</a></td><td>" + i.username + "</td>" +
						"<td>" + i.companyname + "</td>" + "<td>" + i.phone + "</td>" + "<td>" + i.email + "</td>" + "<td>"+ "<button type='button' onclick='accept("+jsonString+");'>승인</button><button type='button' onclick='deny("+jsonString+");'>거절</button></td></tr>")
					}	
				}
			}
		})
	}
	
	function accept(mvo){
		$("#fountainG").show();
		$.ajax({
			url : "/signup/accept",
			type : "post",
			dataType: "json",
			contentType:"application/json;charset=UTF-8",
			data : JSON.stringify(mvo),
			success : function(data){
				$("#fountainG").hide();
				if(data == true){
					alert(mvo.userid + "님의 기업 회원가입을 승인했습니다.");
					location.reload();
				}else{
					alert("에러가 발생했습니다. 잠시 후 다시 시도하세요.");
					location.reload();
				}
			}
		});
	}
	
	function deny(mvo){
		$("#fountainG").show();
		$.ajax({
			url : "/signup/deny",
			type : "post",
			dataType: "json",
			contentType:"application/json;charset=UTF-8",
			data : JSON.stringify(mvo),
			success : function(data){
				$("#fountainG").hide();
				if(data == true){
					alert(mvo.userid + "님의 기업 회원가입을 거절했습니다.");
					location.reload();
				}else{
					alert("에러가 발생했습니다. 잠시 후 다시 시도하세요.");
					location.reload();
				}
			}
		});
	}
	
	function getCompany(){
		$("#companyTable>tbody").empty();
		$("#waitTable").hide();
		$("#companyTable").show();
		$("#page_selection").show();
		$(".searchList").show();
		$("#companyTable>thead").html("<tr><th>기업명</th><th>대표자</th><th>설립일</th><th>주소</th></tr>");
		$.ajax({
			url : "/admin/company/list",
			type : "post",
			success : function(data){
				console.log(data);
				if(data.companyList.length == 0){
					$('#companyTable').append('<tr><td colspan="4">조회한 결과가 없습니다.</td></tr>');
				}else{
					for(var i of data.companyList){
						console.log("dd");
						$('#companyTable>tbody').append('<tr><td><a href="/company/'+i.id+'">'+i.raw_name+'</a></td><td>'+i.owner+'</td>'+
								'<td>'+i.permit_date+'</td><td>'+i.address+'</td></tr>');
					}	
				}
			}
		})
	}
	
</script>
<body>
	<jsp:include page="/WEB-INF/views/include/nav.jsp" />
	<div class="row row-offcanvas row-offcanvas-left">
		<!-- 좌측 메뉴 사이드 -->
		<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
			role="navigation">
			<div class="sidebar-nav">
				<ul class="nav">
					<li class="1 active"><a onclick="getWaitRegisterList();">기업회원 관리</a></li>
					<li><a onclick="getCompany();">기업페이지 조회</a></li>
					<li><a href="/admin/announce">전체 공지</a></li>
					<!-- <li class="nav-divider"></li> -->
				</ul>
			</div>
		</div>
		<div class="col-xs-6 col-sm-9 bodycontainer">
			<!-- loading bar -->
			<div id="fountainG" style="display:none;">
				<div id="fountainG_1" class="fountainG"></div>
				<div id="fountainG_2" class="fountainG"></div>
				<div id="fountainG_3" class="fountainG"></div>
				<div id="fountainG_4" class="fountainG"></div>
				<div id="fountainG_5" class="fountainG"></div>
				<div id="fountainG_6" class="fountainG"></div>
				<div id="fountainG_7" class="fountainG"></div>
				<div id="fountainG_8" class="fountainG"></div>
			</div>
			<!-- 승인대기 관리 테이블 -->
			<div class="tabContent">
				<table id="waitTable" class="table table-bordered">
				</table>
				<!-- 기업페이지 조회 테이블 -->
				<table id="companyTable" class="table table-bordered">
					<thead></thead>
					<tbody></tbody>
				</table>
				<div id="page_selection"></div>
				    <script>
				        // init bootpag
				        var myboot = $('#page_selection').bootpag({
						    total: 20,
						    page: 1,
						    maxVisible: 5,
						    leaps: true,
						    firstLastUse: true,
						    first: '←',
						    last: '→',
						    wrapClass: 'pagination',
						    activeClass: 'active',
						    disabledClass: 'disabled',
						    nextClass: 'next',
						    prevClass: 'prev',
						    lastClass: 'last',
						    firstClass: 'first'
						}).on("page", function(event, num){
							$('#companyTable>tbody').empty();
							var $text = $('#keyword').val();
						    $.ajax({
						    	url : "/admin/companyPaging",
						    	type : "POST",
						    	data : {page : num, keyword : $text},
						    	success : function(data){
						    		//총 페이지의 수
						    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
					    			myboot.bootpag({total: data.totalPage, page : num})
						    		 //데이터 넣기
						    		 $('#companyTable>tbody').empty();
										if(data.companyList.length == 0){
											$('#companyTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
										}
										for(var i of data.companyList){
											$('#companyTable>tbody').append('<tr><td><a href="/company/'+i.id+'">'+i.raw_name+'</a></td><td>'+i.owner+'</td>'+
												'<td>'+i.permit_date+'</td><td>'+i.address+'</td></tr>');	
										}
						    	}
						    })
						})
				        
				        var repaging = function(event, num){
				        	$('#companyTable>tbody').empty();
							var $text = $('#keyword').val();
							var $option = $('#optionSelect').val();
						    $.ajax({
						    	url : "/admin/companyPaging",
						    	type : "POST",
						    	data : {page : num, keyword : $text, option : $option},
						    	success : function(data){
						    		//총 페이지의 수
						    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
						    		myboot.bootpag({total: data.totalPage, page : num})
						    		 //데이터 넣기
										if(data.companyList.length == 0){
											$('#companyTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
										}
										for(var i of data.companyList){
											$('#companyTable>tbody').append('<tr><td><a href="/company/'+i.id+'">'+i.raw_name+'</a></td><td>'+i.owner+'</td>'+
													'<td>'+i.permit_date+'</td><td>'+i.address+'</td></tr>');
										}
						    	}
						    })
						}
				        
				    </script>
				    <div class="searchList">
					<div class="form-group">
						<input type="text" id="keyword" name="keyword" onkeypress="if(event.keyCode==13) {repaging(event, 1);};"
							class="form-control" placeholder="검색어를 입력하세요." />
						<div class="btn_search">
							<button type="button" onclick='repaging(event, 1);' 
								class="btn btn_search2">검색</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>