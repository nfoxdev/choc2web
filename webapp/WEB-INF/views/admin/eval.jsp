<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/loading.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/company.css">
<body>
	<h1>기업회원 가입</h1>
	<form:form modelAttribute="memberVo" action="/admin/eval" method="post">
		<ul>
		      <li class="text">아이디</li>
		      <li>
			      <label><span>${memberVo.userid }</span></label>
		      </li>
		      <li class="text">이름</li>
		      <li>
		      	  <label><span>${memberVo.username }</span></label>
		      </li>
		    	  <li class="text">전화번호</li>
		      <li>
		     	<form:input type="text" path="phone" value="${memberVo.phone}" maxlength="20"/>
		      	<form:errors path="phone" class="errors"></form:errors>
		      </li>
		      <li class="text">이메일</li>
		      <li>
		      	<form:input type="email" path="email" value="${memberVo.email}" maxlength="40"/>
		      	<form:errors path="email" class="errors"></form:errors>
		      </li>
		      <li class="text">소속</li>
		      <li>
		      	<form:input type="text" path="raw_name" value="${memberVo.raw_name}"/><form:errors path="raw_name" class="errors"></form:errors>
		      </li>
		      <li><input type="submit" onclick="loading();"><a class="btn btn-default" href="/admin">취소</a></li>
		</ul>
	</form:form>
			<!-- loading bar -->
			<div id="fountainG" style="display:none;">
				<div id="fountainG_1" class="fountainG"></div>
				<div id="fountainG_2" class="fountainG"></div>
				<div id="fountainG_3" class="fountainG"></div>
				<div id="fountainG_4" class="fountainG"></div>
				<div id="fountainG_5" class="fountainG"></div>
				<div id="fountainG_6" class="fountainG"></div>
				<div id="fountainG_7" class="fountainG"></div>
				<div id="fountainG_8" class="fountainG"></div>
			</div>
</body>
<script>
	function loading(){
		$("#fountainG").show();
	}
</script>
</html>