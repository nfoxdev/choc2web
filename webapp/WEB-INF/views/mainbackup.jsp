<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>:::choc2:::</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<body>
	<jsp:include page="/WEB-INF/views/include/nav.jsp" />
<div class="container">
	<div class="row text-center">
		<div class="col-sm-4"><a class="btn btn-primary btn-block" href="">카테고리로 찾기</a></div>		
		<div class="col-sm-4"><a class="btn btn-primary btn-block" href="/brand">브랜드로 찾기</a></div>
		<div class="col-sm-4"><a class="btn btn-primary btn-block" href="">피부타입으로 찾기</a></div>		
	</div>
	
	<h1 style="text-align: center;">::: HOT ITEM :::</h1>
	<div class="searchList">
		<div class="form-group">
			<select name="option" class="form-control" id="optionSelect">
				<option value="all" selected="selected">전체</option>
				<option value="cos_name">제품명</option>
				<option value="brand_name">브랜드</option>
			</select>
			<input type="text" id="keyword" name="keyword" onkeypress="if(event.keyCode==13) {repaging(event, 1);};"
				class="form-control" placeholder="찾고 싶은 제품이 있나요?" />
			<div class="btn_search">
				<button type="button" onclick='repaging(event, 1);' 
					class="btn btn_search2">검색</button>
			</div>
		</div>
	</div>
	<div class="row hotitemTableDiv col-sm-10 col-sm-offset-1">
        <table id="mainCosTable" class="table table-bordered table-striped">
        
		<c:if test="${empty cosmeticList }">
			<tr>
				<td colspan="5">조회한 결과가 없습니다.</td>
			</tr>
		</c:if>
		<c:forEach var="cos" items="${cosmeticList }">
			<tr>
				<td width="20%" style="line-height:0"><img src="${cos.img_path }" style="width:120px; height:120px; margin:0; padding:0;"/></td>
				<td width="30%">${cos.kor_name }</td>
				<td width="30%">${cos.brand.kor_name }</td>
				<td width="10%">
					<c:choose>
						<c:when test="${cos.functional == true }">기능성 있음</c:when>
						<c:otherwise>기능성 없음</c:otherwise>
					</c:choose>
				</td>
				<td width="10%"><a class="btn btn-sm btn-success" href="${cos.link }">제품 링크</a></td>
			</tr>
		</c:forEach>
	</table>
    </div>
	<div class="row col-sm-2 col-sm-offset-10">
		<a class="btn btn-danger btn-lg pull-right" href="/brand">-> 더보기</a>
	</div>
	<div id="page_selection"></div>
	    <script>
	        // init bootpag
	        var myboot = $('#page_selection').bootpag({
			    total: ${boardMap.totalPage},
			    page: 1,
			    maxVisible: 5,
			    leaps: true,
			    firstLastUse: true,
			    first: '←',
			    last: '→',
			    wrapClass: 'pagination',
			    activeClass: 'active',
			    disabledClass: 'disabled',
			    nextClass: 'next',
			    prevClass: 'prev',
			    lastClass: 'last',
			    firstClass: 'first'
			}).on("page", function(event, num){
				var $text = $('#keyword').val();
				var $option = $('#optionSelect').val();
				var $company_id = ${boardMap.company_id};
			    $.ajax({
			    	url : "/board/boardPaging",
			    	type : "POST",
			    	data : {page : num, keyword : $text, option : $option, company_id : $company_id},
			    	success : function(data){
			    		//총 페이지의 수
			    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
		    			myboot.bootpag({total: data.totalPage, page : num})
			    		 //데이터 넣기
			    		 $('#mainCosTable>tbody').empty();
							if(data.boardList.length == 0){
								$('#mainCosTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
							}
							for(var i of data.boardList){
								$('#mainCosTable>tbody').append('<tr><td>'+i.idx+'</td><td><a href="/board/view?id='+i.idx+'">'+i.title+'</td><td>'+i.userid+'</td>'+
									'<td>'+i.regdate+'</td><td>'+i.hit+'</td></tr>');	
							}
			    	}
			    })
			})
	        
	        var repaging = function(event, num){
				var $text = $('#keyword').val();
				var $option = $('#optionSelect').val();
			    $.ajax({
			    	url : "/board/boardPaging",
			    	type : "POST",
			    	data : {page : num, keyword : $text, option : $option},
			    	success : function(data){
			    		//총 페이지의 수
			    		//현재 페이지부터의 한페이지에 출력할 로우량만큼의 데이터리스트
			    		myboot.bootpag({total: data.totalPage, page : num})
			    		 //데이터 넣기
			    		 $('#mainCosTable>tbody').empty();
							if(data.boardList.length == 0){
								$('#mainCosTable>tbody').append('<tr><td colspan="5">조회한 결과가 없습니다.</td></tr>');
							}
							for(var i of data.boardList){
								$('#mainCosTable>tbody').append('<tr><td>'+i.idx+'</td><td><a href="/board/view?id='+i.idx+'">'+i.title+'</a></td><td>'+i.userid+'</td>'+
									'<td>'+i.regdate+'</td><td>'+i.hit+'</td></tr>');
							}
			    	}
			    })
			}
	        
	    </script>

	    <div class="row col-sm-12">
	    	<br />
	    	<br />
	    	<br />
	    	<br />
	    	<br />
	    	<a class="btn btn-primary btn-block" href="#">피부 상태 분석
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
			</a>
	    </div>
	    <br />
	    <br />
	    
</div>
</body>
</html>